﻿using RecognitionSystemProtocol2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CheckClient2
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        RecognitionSystemClient2 client = new RecognitionSystemClient2();

        public MainWindow()
        {
            InitializeComponent();
            client.GetNewSignalsUpdate += Client_GetNewSignalsUpdate;
        }

        private void Client_GetNewSignalsUpdate(GetNewSignalsResponse answer)
        {
            Console.Beep();
            Console.WriteLine(answer);
        }

        private async void Button_Click(object sender, RoutedEventArgs e)
        {
            await client.IQConnectToServer("127.0.0.1", 36000);
        }

        private async void Button_Click_0(object sender, RoutedEventArgs e)
        {
            var result = await client.GetActiveListFriendlyUAVs();
        }

        private async void Button_Click_1(object sender, RoutedEventArgs e)
        {
            string s = "abcabcabcbacaa";

            Active active = new Active()
            {
                TableID = 1,
                FreqMinMHz = 5700,
                BandwidthMHz = 10,
                isActive = 1
            };

            List<Active> ff = new List<Active>();
            ff.Add(active);

            UAVandText uav = new UAVandText()
            {
                ID = CharByteParseID(s)
            };

            var result = await client.AddFriendlyUAVs(uav, ff);


            s = "abcabcabcbaccc";

            active = new Active()
            {
                TableID = 2,
                FreqMinMHz = 2700,
                BandwidthMHz = 5,
                isActive = 0
            };

            ff = new List<Active>();
            ff.Add(active);

            uav = new UAVandText()
            {
                ID = CharByteParseID(s)
            };

            result = await client.AddFriendlyUAVs(uav, ff);
        }

        private byte[] CharByteParseID(string value)
        {
            var charArray = value.ToCharArray();

            var bytes = charArray.Select(x => Convert.ToByte(x)).ToArray();

            return bytes;
        }

        private async void Button_Click_2(object sender, RoutedEventArgs e)
        {
            string s = "abcabcabcbacaa";

            Active active = new Active()
            {
                TableID = 1,
                FreqMinMHz = 2400,
                BandwidthMHz = 20,
                isActive = 0
            };

            Active active2 = new Active()
            {
                TableID = 2,
                FreqMinMHz = 2800,
                BandwidthMHz = 10,
                isActive = 0
            };

            List<Active> ff = new List<Active>();
            ff.Add(active);
            ff.Add(active2);

            UAVandText uav = new UAVandText()
            {
                ID = CharByteParseID(s)
            };

            var result = await client.SetFriendlyUAVs(uav, ff);
        }

        private async void Button_Click_3(object sender, RoutedEventArgs e)
        {
            string s = "abcabcabcbacaa";

            var result = await client.DeleteFriendlyUAV(CharByteParseID(s));
        }

        private async void Button_Click_4(object sender, RoutedEventArgs e)
        {
            var result = await client.DeleteFriendlyUAVs();
        }

        private async void Button_Click_5(object sender, RoutedEventArgs e)
        {
            string s = "abcabcabcbacaa";

            isGainUAV isGainUAV = new isGainUAV()
            {
                GainUAV = new UAV()
                { 
                ID = CharByteParseID(s),
                TableID = 3,
                FreqMinMHz = 5555,
                BandwidthMHz = 5,
                },
                ValueAntGain = 16,
                ValuePreselGain = 22,
            };

            var result = await client.RecordSignalUAV(isGainUAV);
        }

        private async void Button_Click_6(object sender, RoutedEventArgs e)
        {
            logView.AddLog("Отправка 7");
            var result = await client.AffiliationUAV(1234, 4444, 4, 12, 13);
            logView.AddLog("Ответ 7");
        }

        private async void Button_Click_7(object sender, RoutedEventArgs e)
        {
            var result = await client.GetFFT(5400);
        }

        private async void Button_Click_8(object sender, RoutedEventArgs e)
        {
            var result = await client.SetUSRPn310Gain(21);
        }

        private async void Button_Click_9(object sender, RoutedEventArgs e)
        {
            var result = await client.ChooseUSRPn310Chanel(2);
        }

        private async void Button_Click_10(object sender, RoutedEventArgs e)
        {
            var result = await client.OnOffAGC(1);
        }

        private async void Button_Click_12(object sender, RoutedEventArgs e)
        {
            logView.AddLog("Отправка 12");
            var result = await client.RequestNewSignals(0);
            logView.AddLog("Ответ 12");
        }

        private async void Button_Click_17(object sender, RoutedEventArgs e)
        {
            logView.AddLog("Отправка 17");
            var result = await client.RequestNewSignalsDetail(0);
            logView.AddLog("Ответ 17");
        }
    }
}
