﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace LogViewer
{
    /// <summary>
    /// Interaction logic for LogView.xaml
    /// </summary>
    public partial class LogView : UserControl
    {
        private int _index = 0;

        public ObservableCollection<LogEntry> LogEntries { get; set; }

        public string AddInfo { get; set; } = "";

        public LogView()
        {
            InitializeComponent();

            DataContext = LogEntries = new ObservableCollection<LogEntry>();
        }

        public void AddLog(string message)
        {
            Dispatcher.BeginInvoke((Action)(() => LogEntries.Add(GetLogEntry(message))));
        }

        public void Clear()
        {
            LogEntries.Clear();
        }

        private LogEntry GetLogEntry(string message)
        {
            return new LogEntry
            {
                Index = _index++,
                DateTime = DateTime.Now,
                Message = message
            };
        }

        private void ScrollViewer_OnScrollChanged(object sender, ScrollChangedEventArgs e)
        {
            ScrollViewer sv = sender as ScrollViewer;
            bool AutoScrollToEnd = true;
            if (sv.Tag != null)
            {
                AutoScrollToEnd = (bool)sv.Tag;
            }
            if (e.ExtentHeightChange == 0)// user scroll
            {
                AutoScrollToEnd = sv.ScrollableHeight == sv.VerticalOffset;
            }
            else// content change
            {
                if (AutoScrollToEnd)
                {
                    sv.ScrollToEnd();
                }
            }
            sv.Tag = AutoScrollToEnd;
        }

        private void Export_OnClick(object sender, RoutedEventArgs e)
        {
            StringBuilder sb = new StringBuilder();
            foreach (var log in LogEntries)
            {
                sb.Append(log.DateTime + " " + log.Message + "\r\n");
            }

            SaveFile(sb.ToString(), "Export", ".txt", AddInfo + "-");
        }

        private void SaveFile(string content, string folderName = "Export", string extension = ".csv", string addToFileName = "")
        {
            if (content == null) return;

            DateTime dateTime = DateTime.Now;

            string path = System.Diagnostics.Process.GetCurrentProcess().MainModule.FileName;

            path = path.Remove(path.LastIndexOf('\\') + 1);

            string dirPath = path + folderName;
            DirectoryInfo dirInfo = new DirectoryInfo(dirPath);
            if (!dirInfo.Exists)
            {
                dirInfo.Create();
            }

            string subFolderName = dateTime.ToString("dd.MM.yyyy");
            string subDirPath = dirPath + "\\" + subFolderName;
            DirectoryInfo subDirinfo = new DirectoryInfo(subDirPath);
            if (!subDirinfo.Exists)
            {
                subDirinfo.Create();
            }

            string timeString = dateTime.ToString("hh-mm-ss-tt", new CultureInfo("en-US"));

            string FullPath = subDirPath + "\\" + addToFileName + timeString + extension;

            using (StreamWriter sw = new StreamWriter(FullPath, false, System.Text.Encoding.Default))
            {
                sw.WriteLine(content);
                sw.Close();
            }

        }

        private void Clear_OnClick(object sender, RoutedEventArgs e)
        {
            Clear();
        }
    }
}
