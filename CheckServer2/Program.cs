﻿using RecognitionSystemProtocol2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Diagnostics;
using System.Windows.Forms;
using System.Threading;

namespace CheckServer2
{
    class Program
    {
        static RecognitionSystemServer server = new RecognitionSystemServer();

        static Random random = new Random();

        static void Main(string[] args)
        {
            var ip = Console.ReadLine();
            if (ip == string.Empty)
            {
                server.ServerStart();
            }
            else { server.ServerStart(ip, 36000); }

            server.ServerRequest += Server_ServerRequest;

            Application.Run();
        }



        private static async void Server_ServerRequest(int Response, llcss.IBinarySerializable Request)
        {
            switch (Response)
            {
                //case 1:

                //    SetActiveListFriendlyUAVsRequest request = (SetActiveListFriendlyUAVsRequest)Request;

                //    List<IsFileSignalUAV> isFileSignalUAVsList = new List<IsFileSignalUAV>();

                //    foreach (var IsActiveUAV in request.IsActiveUAVs)
                //    {
                //        IsFileSignalUAV isFileSignalUAV = new IsFileSignalUAV()
                //        {
                //            FileSignalUAV = IsActiveUAV.ActiveUAV,
                //            isFileSignal = IsActiveUAV.isActive
                //        };

                //        isFileSignalUAVsList.Add(isFileSignalUAV);
                //    }

                //    await server.ServerSendFileSignalUAVResponse(isFileSignalUAVsList);

                //    break;

                case 1:

                    bool testError = true;

                    if (testError)
                    {
                        MessageHeader header = new MessageHeader()
                        {
                            Code = 1,
                            ErrorCode = 1,
                            InformationLength = 0,
                            ReceiverAddress = 0,
                            SenderAddress = 0
                        };

                        GetActiveListFriendlyUAVsResponse errorResponse = new GetActiveListFriendlyUAVsResponse();

                        var b1 = errorResponse.GetBytes();

                        errorResponse.Header = header;

                        var b2 = errorResponse.GetBytes();

                        await server.ServerSendTestResponse(header.GetBytes());
                    }

                    break;

                case 2:
                    await server.ServerSendAddFriendlyUAVResponse();
                    break;
                case 3:
                    await server.ServerSendDeleteFriendlyUAVResponse();
                    break;
                case 4:
                    await server.ServerSendDeleteFriendlyUAVsResponse();
                    break;
                case 6:
                    await server.ServerDefaultResponse(6);
                    break;
                case 7:
                    await server.ServerDefaultResponse(7);
                    break;


                //case 7:

                //    testError = false;
                //    if (testError)
                //    {
                //        MessageHeader header = new MessageHeader()
                //        {
                //            Code = 7,
                //            ErrorCode = 1,
                //            InformationLength = 0,
                //            ReceiverAddress = 0,
                //            SenderAddress = 0
                //        };

                //        AffiliationUAVResponse errorResponse = new AffiliationUAVResponse();

                //        var b1 = errorResponse.GetBytes();

                //        errorResponse.Header = header;

                //        var b2 = errorResponse.GetBytes();

                //        await server.ServerSendTestResponse(header.GetBytes());
                //    }

                //    Console.WriteLine(DateTime.Now.TimeOfDay.ToString("hh\\:mm\\:ss") + " Received 7");
                //    Task.Run(() => Answer7());


                //    break;

                case 8:

                    testError = true;

                    if (testError)
                    {
                        MessageHeader header = new MessageHeader()
                        {
                            Code = 8,
                            ErrorCode = 1,
                            InformationLength = 0,
                            ReceiverAddress = 0,
                            SenderAddress = 0
                        };

                        GetFFTResponce errorResponse = new GetFFTResponce();

                        var b1 = errorResponse.GetBytes();

                        errorResponse.Header = header;

                        var b2 = errorResponse.GetBytes();

                        await server.ServerSendTestResponse(header.GetBytes());
                    }

                    break;

                case 9:
                    await server.ServerGainResponse();
                    break;
                //case 5:

                //    UAVType uAVType = (UAVType)random.Next(0,3);

                //    byte[] bytes = new byte[14];
                //    bytes = bytes.Select(x => x = (byte)random.Next(48, 128)).ToArray();

                //    await server.ServerSendAffiliationUAVResponse(uAVType, bytes);
                case 12:

                    //FoundedNewObjectsRequest request = (FoundedNewObjectsRequest)Request;

                    //List<NewObject> objects = new List<NewObject>();
                    //if (request.FrequencyNumber == 1)
                    //{
                    //    objects.Add(new NewObject(1, 2445, 20));
                    //    objects.Add(new NewObject(1, 2440.77777f, 10));
                    //}
                    //else
                    //{
                    //    objects.Add(new NewObject(1, 5805, 5));
                    //}

                    //await server.ServerSendNewObjectsResponse(objects);

                    bool newTest = true;
                    if (!newTest)
                    {
                        testError = true;

                        if (!testError)
                        {
                            await server.ServerSendNewObjectsResponse((byte) random.Next(0, 2),
                                (byte) random.Next(0, 2));
                        }
                        else
                        {
                            MessageHeader header = new MessageHeader()
                            {
                                Code = 12,
                                ErrorCode = 1,
                                InformationLength = 0,
                                ReceiverAddress = 0,
                                SenderAddress = 0
                            };

                            GetNewSignalsResponse errorResponse = new GetNewSignalsResponse();

                            var b1 = errorResponse.GetBytes();

                            errorResponse.Header = header;

                            var b2 = errorResponse.GetBytes();

                            //await server.ServerSendTestResponse(header.GetBytes());

                            MessageHeader header2 = new MessageHeader()
                            {
                                Code = 14,
                                ErrorCode = 0,
                                InformationLength = 2,
                                ReceiverAddress = 0,
                                SenderAddress = 0
                            };

                            GetNewSignalsResponse errorResponse2 = new GetNewSignalsResponse();

                            errorResponse2.Header = header2;

                            await server.ServerSendTestResponse(errorResponse2.GetBytes());
                        }
                    }
                    else
                    {
                        Console.WriteLine(DateTime.Now.TimeOfDay.ToString("hh\\:mm\\:ss") + " Received 12");
                        Task.Run(() => Answer12());
                    }


                    break;
                case 13:

                    await server.ServerFinishedRebooting();

                    break;
                case 16:
                    Thread.Sleep(5000);
                    await server.ServerDefaultResponse(16);
                    break;


                case 17:

                   
                    bool newTestDetail = true;
                    if (!newTestDetail)
                    {
                        testError = true;

                        if (!testError)
                        {
                            await server.ServerSendNewObjectsResponse((byte)random.Next(0, 2),
                                (byte)random.Next(0, 2));
                        }
                        else
                        {
                            MessageHeader header = new MessageHeader()
                            {
                                Code = 17,
                                ErrorCode = 1,
                                InformationLength = 0,
                                ReceiverAddress = 0,
                                SenderAddress = 0
                            };

                            GetNewDetailSignalsResponse errorResponse = new GetNewDetailSignalsResponse();

                            var b1 = errorResponse.GetBytes();

                            errorResponse.Header = header;

                            var b2 = errorResponse.GetBytes();

                            //await server.ServerSendTestResponse(header.GetBytes());

                            MessageHeader header2 = new MessageHeader()
                            {
                                Code = 17,
                                ErrorCode = 0,
                                InformationLength = 2,
                                ReceiverAddress = 0,
                                SenderAddress = 0
                            };

                            GetNewDetailSignalsResponse errorResponse2 = new GetNewDetailSignalsResponse();

                            errorResponse2.Header = header2;

                            await server.ServerSendTestResponse(errorResponse2.GetBytes());
                        }
                    }
                    else
                    {
                        Console.WriteLine(DateTime.Now.TimeOfDay.ToString("hh\\:mm\\:ss") + " Received 17");
                        Task.Run(() => Answer17());
                    }


                    break;

                default:
                    Console.WriteLine(Response);
                    break;
            }
        }

        private static async Task Answer7()
        {
            await Task.Delay(3000);

            MessageHeader header = new MessageHeader()
            {
                Code = 7,
                ErrorCode = 0,
                InformationLength = 19,
                ReceiverAddress = 0,
                SenderAddress = 0
            };

            AffiliationUAVResponse response = new AffiliationUAVResponse();
            response.Header = header;

            var bytes = response.GetBytes();

            await server.ServerSendTestResponse(response.GetBytes());

            Console.WriteLine(DateTime.Now.TimeOfDay.ToString("hh\\:mm\\:ss") + " Send 7");
        }

        private static async Task Answer12()
        {
            await Task.Delay(1000);

            MessageHeader header = new MessageHeader()
            {
                Code = 12,
                ErrorCode = 0,
                InformationLength = 2,
                ReceiverAddress = 0,
                SenderAddress = 0
            };

            GetNewSignalsResponse response = new GetNewSignalsResponse();
            response.Header = header;

            var bytes = response.GetBytes();

            await server.ServerSendTestResponse(response.GetBytes());

            Console.WriteLine(DateTime.Now.TimeOfDay.ToString("hh\\:mm\\:ss") + " Send 12");
        }


        private static async Task Answer17()
        {
            await Task.Delay(1000);

            MessageHeader header = new MessageHeader()
            {
                Code = 17,
                ErrorCode = 0,
                InformationLength = 2,
                ReceiverAddress = 0,
                SenderAddress = 0
            };

            GetNewDetailSignalsResponse response = new GetNewDetailSignalsResponse();
            response.Header = header;

            var bytes = response.GetBytes();

            await server.ServerSendTestResponse(response.GetBytes());

            Console.WriteLine(DateTime.Now.TimeOfDay.ToString("hh\\:mm\\:ss") + " Send 17");
        }



    }
}
