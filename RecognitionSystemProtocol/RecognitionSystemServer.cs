﻿using llcss;
using Nito.AsyncEx;
using RecognitionSystemProtocol2;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace RecognitionSystemProtocol
{
    public class RecognitionSystemServer
    {
        private int port = 36000;
        private TcpListener listener;

        private List<ClientObject> ListOfClientObjects = new List<ClientObject>();

        public delegate void ResponseIBinarySerializableServerRequest(int Response, IBinarySerializable Request);
        public event ResponseIBinarySerializableServerRequest ServerRequest;

        List<(int, int)> Responses = new List<(int, int)>();

        public async Task ServerStart()
        {
            listener = new TcpListener(IPAddress.Parse("127.0.0.1"), port);
            listener.Start();
            Console.WriteLine($"Server started on {IPAddress.Parse("127.0.0.1")}:{port}");
            Console.WriteLine("Waiting for connections...");
            while (true)
            {
                TcpClient client = await listener.AcceptTcpClientAsync();
                ClientObject clientObject = new ClientObject(client);
                ListOfClientObjects.Add(clientObject);

                InitClientObjectEvents(ref clientObject);

                Console.WriteLine("Client connected");

                //создаем новый поток для обслуживания нового клиента
                Task.Run(() => clientObject.Read());
                var answer = await ServerSendAddressIdentificationRequest();
            }
        }

        public async Task ServerStart(string IP, int port)
        {
            try
            {
                //Пробуем стартовать сервер
                listener = new TcpListener(IPAddress.Parse(IP), port);
                listener.Start();
                Console.WriteLine($"Server started on {IP}:{port}");
                Console.WriteLine("Waiting for connections...");
            }
            catch
            {
                listener = new TcpListener(IPAddress.Parse("127.0.0.1"), port);
                listener.Start();
                Console.WriteLine($"Server started on {IPAddress.Parse("127.0.0.1")}:{port}");
                Console.WriteLine("Waiting for connections...");
            }
            while (true)
            {
                TcpClient client = await listener.AcceptTcpClientAsync();
                ClientObject clientObject = new ClientObject(client);
                ListOfClientObjects.Add(clientObject);

                InitClientObjectEvents(ref clientObject);

                Console.WriteLine("Client connected");

                //создаем новый поток для обслуживания нового клиента
                Task.Run(() => clientObject.Read());
                var answer = await ServerSendAddressIdentificationRequest();
            }
        }

        private void InitClientObjectEvents(ref ClientObject clientObject)
        {
            clientObject.Request += ClientObject_Request;
        }

        private void ClientObject_Request(int Adress, int Response, IBinarySerializable Request)
        {
            lock (Responses)
            {
                Responses.Add((Response, Adress));
            }
            ServerRequest?.Invoke(Response, Request);
        }

        //Шифр 0
        public async Task<List<DefaultMessage>> ServerSendAddressIdentificationRequest()
        {
            List<DefaultMessage> ListOfDefaultMessages = new List<DefaultMessage>();
            for (int i = 0; i < ListOfClientObjects.Count; i++)
            {
                var result = await ListOfClientObjects[i].SendAddressIdentificationRequest(Convert.ToByte(i + 1));
                ListOfDefaultMessages.Add(result);
            }
            return ListOfDefaultMessages;
        }

        //Шифр 1
        public async Task<DefaultMessage> ServerSendFileSignalUAVResponse(IEnumerable<IsFileSignalUAV> isFileSignalUAVs)
        {
            int i = 0;
            lock (Responses)
            {
                var el = Responses.First(x => x.Item1 == 1);
                i = el.Item2 - 1;
                Responses.Remove(el);
            }

            var result = await ListOfClientObjects[i].SendFileSignalUAVResponse(isFileSignalUAVs);
            return result;
        }

        //Шифр 2
        public async Task<DefaultMessage> ServerSendAddFriendlyUAVResponse()
        {
            int i = 0;
            lock (Responses)
            {
                var el = Responses.First(x => x.Item1 == 2);
                i = el.Item2 - 1;
                Responses.Remove(el);
            }

            var result = await ListOfClientObjects[i].SendAddFriendlyUAVResponse();
            return result;
        }

        //Шифр 3
        public async Task<DefaultMessage> ServerSendDeleteFriendlyUAVResponse()
        {
            int i = 0;
            lock (Responses)
            {
                var el = Responses.First(x => x.Item1 == 3);
                i = el.Item2 - 1;
                Responses.Remove(el);
            }

            var result = await ListOfClientObjects[i].SendDeleteFriendlyUAVResponse();
            return result;
        }

        //Шифр 4
        public async Task<DefaultMessage> ServerSendDeleteFriendlyUAVsResponse()
        {
            int i = 0;
            lock (Responses)
            {
                var el = Responses.First(x => x.Item1 == 4);
                i = el.Item2 - 1;
                Responses.Remove(el);
            }

            var result = await ListOfClientObjects[i].SendDeleteFriendlyUAVsResponse();
            return result;
        }

        //Шифр 5
        public async Task<DefaultMessage> ServerSendAffiliationUAVResponse(UAVType Type, IEnumerable<byte> ID)
        {
            int i = 0;
            lock (Responses)
            {
                var el = Responses.First(x => x.Item1 == 5);
                i = el.Item2 - 1;
                Responses.Remove(el);
            }

            var result = await ListOfClientObjects[i].SendAffiliationUAVResponse(Type, ID);
            return result;
        }

    }

    public class ClientObject
    {
        public TcpClient client;

        private readonly AsyncLock asyncLock;

        public delegate void IsConnectedEventHandler(bool isConnected);
        public event IsConnectedEventHandler ClientIsConnected;

        public delegate void OnReadEventHandler(bool isRead);
        public event OnReadEventHandler ClientIsRead;

        public delegate void IsWriteEventHandler(bool isWrite);
        public event IsWriteEventHandler ClientIsWrite;

        public delegate void AdressAndResponseIBinarySerializableRequest(int Adress, int Response, IBinarySerializable Request);
        public event AdressAndResponseIBinarySerializableRequest Request;

        public int ClientAdress = 0;

        public ClientObject(TcpClient tcpClient)
        {
            client = tcpClient;
            asyncLock = new AsyncLock();
        }


        public async Task Read()
        {
            while (client.Connected)
            {
                var headerBuffer = new byte[MessageHeader.BinarySize];
                var header = new MessageHeader();
                var count = 0;

                try
                {
                    count = await client.GetStream().ReadAsync(headerBuffer, 0, MessageHeader.BinarySize).ConfigureAwait(false);
                    ClientIsRead?.Invoke(true);
                }
                catch (Exception)
                {
                    //Console.WriteLine("Сервер отвалился");
                    ClientIsRead?.Invoke(false);
                    ClientIsConnected?.Invoke(false);
                    //break;
                }
                if (count != MessageHeader.BinarySize)
                {
                    //fatal error
                    //break;
                    ClientIsRead?.Invoke(false);
                    return;
                }

                MessageHeader.TryParse(headerBuffer, out header);

                switch (header.Code)
                {
                    case 0:
                        Console.WriteLine(headerBuffer);
                        break;
                    case 1:
                        var response1 = await ReadResponse<SetActiveListFriendlyUAVsRequest>(header, headerBuffer).ConfigureAwait(false);
                        Request?.Invoke(ClientAdress, header.Code, response1);
                        break;
                    case 2:
                        var response2 = await ReadResponse<AddFriendlyUAVRequest>(header, headerBuffer).ConfigureAwait(false);
                        Request?.Invoke(ClientAdress, header.Code, response2);
                        break;
                    case 3:
                        var response3 = await ReadResponse<DeleteFriendlyUAVRequest>(header, headerBuffer).ConfigureAwait(false);
                        Request?.Invoke(ClientAdress, header.Code, response3);
                        break;
                    case 4:
                        var response4 = await ReadResponse<DefaultMessage>(header, headerBuffer).ConfigureAwait(false);
                        Request?.Invoke(ClientAdress, header.Code, response4);
                        break;
                    case 5:
                        var response5 = await ReadResponse<AffiliationUAVRequest>(header, headerBuffer).ConfigureAwait(false);
                        Request?.Invoke(ClientAdress, header.Code, response5);
                        break;
                    default:
                        //вывод полезной информации
                        break;
                }
            }
        }


        private async Task<T> ReadResponse<T>(MessageHeader header, byte[] headerBuffer) where T : class, IBinarySerializable, new()
        {
            var count = 0;
            var buffer = new byte[MessageHeader.BinarySize + header.InformationLength];
            headerBuffer.CopyTo(buffer, 0);

            if (header.InformationLength != 0)
            {
                var difference = header.InformationLength;
                var shiftpos = 0;

                while (difference != 0)
                {
                    count = await client.GetStream().ReadAsync(buffer, MessageHeader.BinarySize + shiftpos, difference).ConfigureAwait(false);
                    if (ClientIsRead != null)
                        ClientIsRead(true);
                    shiftpos += count;
                    difference = difference - count;
                }
            }

            try
            {
                var result = new T();
                result.Decode(buffer, 0);
                return result;
            }
            catch (Exception)
            {
                return null;
            }
        }

        static MessageHeader GetMessageHeader(int code, int length)
        {
            return new MessageHeader(0, 1, (byte)code, 0, length);
        }

        private async Task SendDefault(MessageHeader header)
        {
            using (await asyncLock.LockAsync().ConfigureAwait(false))
            {
                try
                {
                    await client.GetStream().WriteAsync(header.GetBytes(), 0, MessageHeader.BinarySize).ConfigureAwait(false);
                    ClientIsWrite?.Invoke(true);
                }
                catch (Exception)
                {
                    ClientIsWrite?.Invoke(false);
                }
            }
        }

        private async Task SendRequest(MessageHeader header, byte[] message)
        {
            using (await asyncLock.LockAsync().ConfigureAwait(false))
            {
                int count = message.Count();
                try
                {
                    await client.GetStream().WriteAsync(message, 0, count).ConfigureAwait(false);
                    if (ClientIsWrite != null)
                        ClientIsWrite(true);
                }
                catch (Exception)
                {
                    if (ClientIsWrite != null)
                        ClientIsWrite(false);
                }
            }
        }

        private async Task SendResponse<T>(T Response) where T : class, IBinarySerializable, new()
        {
            using (await asyncLock.LockAsync().ConfigureAwait(false))
            {
                try
                {
                    await client.GetStream().WriteAsync(Response.GetBytes(), 0, Response.StructureBinarySize).ConfigureAwait(false);
                    if (ClientIsWrite != null)
                        ClientIsWrite(true);
                }
                catch (Exception)
                {
                    if (ClientIsWrite != null)
                        ClientIsWrite(false);
                }
            }
        }

        //Шифр 0 
        public async Task<DefaultMessage> SendAddressIdentificationRequest(byte ReceiverAddress)
        {
            if (client == null)
                return null;
            if (client.Connected)
            {
                ClientAdress = ReceiverAddress;

                MessageHeader header = GetMessageHeader(code: 0, length: 0);
                header.ReceiverAddress = ReceiverAddress;
                byte[] message = DefaultMessage.ToBinary(header);

                await SendRequest(header, message).ConfigureAwait(false);
                DefaultMessage answer = new DefaultMessage(header);
                return answer;
            }
            else
            {
                MessageHeader header = new MessageHeader(0, 0, 0, 0, 0);
                DefaultMessage answer = new DefaultMessage(header);
                return answer;
            }
        }

        //Шифр 1
        public async Task<DefaultMessage> SendFileSignalUAVResponse(IEnumerable<IsFileSignalUAV> isFileSignalUAVs)
        {
            if (client == null)
                return null;
            if (client.Connected)
            {
                MessageHeader header = GetMessageHeader(code: 1, length: IsFileSignalUAV.BinarySize * isFileSignalUAVs.Count());

                byte[] message = SetActiveListFriendlyUAVsResponse.ToBinary(header, isFileSignalUAVs.ToArray());

                await SendRequest(header, message).ConfigureAwait(false);
                DefaultMessage answer = new DefaultMessage(header);
                return answer;
            }
            else
            {
                MessageHeader header = new MessageHeader(0, 0, 1, 1, 0);
                DefaultMessage answer = new DefaultMessage(header);
                return answer;
            }
        }

        //Шифр 2
        public async Task<DefaultMessage> SendAddFriendlyUAVResponse()
        {
            if (client == null)
                return null;
            if (client.Connected)
            {
                MessageHeader header = GetMessageHeader(code: 2, length: 0);

                byte[] message = DefaultMessage.ToBinary(header);

                await SendRequest(header, message).ConfigureAwait(false);
                DefaultMessage answer = new DefaultMessage(header);
                return answer;
            }
            else
            {
                MessageHeader header = new MessageHeader(0, 0, 2, 1, 0);
                DefaultMessage answer = new DefaultMessage(header);
                return answer;
            }
        }

        //Шифр 3
        public async Task<DefaultMessage> SendDeleteFriendlyUAVResponse()
        {
            if (client == null)
                return null;
            if (client.Connected)
            {
                MessageHeader header = GetMessageHeader(code: 3, length: 0);

                byte[] message = DefaultMessage.ToBinary(header);

                await SendRequest(header, message).ConfigureAwait(false);
                DefaultMessage answer = new DefaultMessage(header);
                return answer;
            }
            else
            {
                MessageHeader header = new MessageHeader(0, 0, 3, 1, 0);
                DefaultMessage answer = new DefaultMessage(header);
                return answer;
            }
        }

        //Шифр 4
        public async Task<DefaultMessage> SendDeleteFriendlyUAVsResponse()
        {
            if (client == null)
                return null;
            if (client.Connected)
            {
                MessageHeader header = GetMessageHeader(code: 4, length: 0);

                byte[] message = DefaultMessage.ToBinary(header);

                await SendRequest(header, message).ConfigureAwait(false);
                DefaultMessage answer = new DefaultMessage(header);
                return answer;
            }
            else
            {
                MessageHeader header = new MessageHeader(0, 0, 4, 1, 0);
                DefaultMessage answer = new DefaultMessage(header);
                return answer;
            }
        }

        //Шифр 5
        public async Task<DefaultMessage> SendAffiliationUAVResponse(UAVType type, IEnumerable<byte> ID)
        {
            if (client == null)
                return null;
            if (client.Connected)
            {
                MessageHeader header = GetMessageHeader(code: 5, length: AffiliationUAVResponse.BinarySize - MessageHeader.BinarySize);

                byte[] message = AffiliationUAVResponse.ToBinary(header, type, ID.ToArray());

                await SendRequest(header, message).ConfigureAwait(false);
                DefaultMessage answer = new DefaultMessage(header);
                return answer;
            }
            else
            {
                MessageHeader header = new MessageHeader(0, 0, 5, 1, 0);
                DefaultMessage answer = new DefaultMessage(header);
                return answer;
            }
        }

        ////Шифр 12
        //public async Task<DefaultMessage> SendNewObjects(IEnumerable<NewObject> objects)
        //{
        //    if (client == null)
        //        return null;
        //    if (client.Connected)
        //    {
        //        RecognitionSystemProtocol2.MessageHeader header = RecognitionSystemProtocol2.GetMessageHeader(code: 12, length: NewObject.BinarySize * objects.Count());

        //        byte[] message = GetNewObjectsListResponse.ToBinary(header, objects.ToArray());

        //        await SendRequest(header, message).ConfigureAwait(false);
        //        DefaultMessage answer = new DefaultMessage(header);
        //        return answer;
        //    }
        //    else
        //    {
        //        MessageHeader header = new MessageHeader(0, 0, 1, 1, 0);
        //        DefaultMessage answer = new DefaultMessage(header);
        //        return answer;
        //    }
        //}
    }
}
