﻿using llcss;
using Nito.AsyncEx;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace RecognitionSystemProtocol
{
    public class RecognitionSystemClient
    {
        private TcpClient client;
        private string IP;
        private int port = 36000;

        private byte ReceiverAddress;

        private readonly AsyncLock asyncLock;

        private readonly ConcurrentDictionary<int, ConcurrentQueue<TaskCompletionSource<IBinarySerializable>>> concurrentDictionary;

        private int _AwaiterTime = 400;
        public int AwaiterTime
        {
            get { return _AwaiterTime; }
            set
            {
                if (_AwaiterTime != value)
                    _AwaiterTime = value;
            }
        }

        private bool disconnect = false;

        private bool _IsConnected = false;
        public bool IsConnected
        {
            get { return _IsConnected; }
            set
            {
                _IsConnected = value;
                OnConnected?.Invoke(value);
            }
        }

        private void IsConnectedChange(bool value)
        {
            IsConnected = value;
        }


        public delegate void OnConnectedEventHandler(bool onConnected);
        public event OnConnectedEventHandler OnConnected;

        public delegate void OnReadEventHandler(bool onRead);
        public event OnReadEventHandler OnRead;

        public delegate void OnWriteEventHandler(bool onWrite);
        public event OnWriteEventHandler OnWrite;

        public RecognitionSystemClient()
        {
            asyncLock = new AsyncLock();
            concurrentDictionary = new ConcurrentDictionary<int, ConcurrentQueue<TaskCompletionSource<IBinarySerializable>>>();
        }

        static byte OwnServerAddress = 0;
        static MessageHeader GetMessageHeader(int code, int length)
        {
            //return new MessageHeader(0, 1, (byte)code, 0, length);
            return new MessageHeader(OwnServerAddress, 1, (byte)code, 0, length);
        }

        private async Task ConnectToServer(string IP)
        {
            disconnect = false;
            client = new TcpClient();

            while (!client.Connected)
            {
                try
                {
                    await client.ConnectAsync(IPAddress.Parse(IP), port).ConfigureAwait(false);

                    this.IP = IP;
                    //this.port = port;
                }
                catch (Exception)
                {

                }
                if (!client.Connected)
                    await Task.Delay(500);
            }

            Console.WriteLine("Connect");

            await OnConnectedTask();

            Task.Run(() => ReadThread());

            IsConnectedChange(true);
        }
        private async Task ConnectToServer(string IP, int port)
        {
            disconnect = false;
            client = new TcpClient();

            while (!client.Connected)
            {
                try
                {
                    await client.ConnectAsync(IPAddress.Parse(IP), port).ConfigureAwait(false);

                    this.IP = IP;
                    this.port = port;
                }
                catch (FormatException)
                {
                    Console.WriteLine(nameof(FormatException));
                    return;
                }
                catch (Exception)
                {

                }
                if (!client.Connected)
                    await Task.Delay(500);
            }

            Console.WriteLine("Connect");

            await OnConnectedTask();

            Task.Run(() => ReadThread());

            IsConnectedChange(true);
        }
        private async Task ConnectToServer2(string IP, CancellationToken token)
        {
            disconnect = false;
            client = new TcpClient();

            while (!client.Connected)
            {
                if (token.IsCancellationRequested)
                    return;
                try
                {
                    await client.ConnectAsync(IPAddress.Parse(IP), port).ConfigureAwait(false);

                    this.IP = IP;
                    //this.port = port;
                }
                catch (Exception)
                {

                }
                if (!client.Connected)
                    await Task.Delay(500);
            }

            Console.WriteLine("Connect2");

            Task.Run(() => ReadThread());

            IsConnectedChange(true);
        }
        private async Task ConnectToServer2(string IP, int port, CancellationToken token)
        {
            disconnect = false;
            client = new TcpClient();

            while (!client.Connected)
            {
                if (token.IsCancellationRequested)
                    return;
                try
                {
                    await client.ConnectAsync(IPAddress.Parse(IP), port).ConfigureAwait(false);

                    this.IP = IP;
                    this.port = port;
                }
                catch (Exception)
                {

                }
                if (!client.Connected)
                    await Task.Delay(500);
            }

            Console.WriteLine("Connect2");

            Task.Run(() => ReadThread());

            IsConnectedChange(true);
        }

        private async Task<string> IQConnectToServer(string IP, int timeout = 3000)
        {
            Regex regex = new Regex(@"^(25[0-5]|2[0-4][0-9]|[0-1][0-9]{2}|[0-9]{2}|[0-9])(\.(25[0-5]|2[0-4][0-9]|[0-1][0-9]{2}|[0-9]{2}|[0-9])){3}$");
            if (regex.IsMatch(IP, 0))
            {
                CancellationTokenSource cts = new CancellationTokenSource();
                CancellationToken token = cts.Token;

                var task = ConnectToServer2(IP, token);

                if (await Task.WhenAny(task, Task.Delay(timeout)) == task)
                {
                    //Console.WriteLine("task completed within timeout");
                    return "OK";
                }
                else
                {
                    cts.Cancel();
                    //Console.WriteLine("timeout logic");
                    return "Invalid IP address or Server not found";
                }
            }
            else
            {
                return "Invalid IP address";
            }
        }
        public async Task<string> IQConnectToServer(string IP, int port, int timeout = 3000)
        {
            Regex regex = new Regex(@"^(25[0-5]|2[0-4][0-9]|[0-1][0-9]{2}|[0-9]{2}|[0-9])(\.(25[0-5]|2[0-4][0-9]|[0-1][0-9]{2}|[0-9]{2}|[0-9])){3}$");
            if (regex.IsMatch(IP, 0))
            {
                CancellationTokenSource cts = new CancellationTokenSource();
                CancellationToken token = cts.Token;

                var task = ConnectToServer2(IP, port, token);

                if (await Task.WhenAny(task, Task.Delay(timeout)) == task)
                {
                    //Console.WriteLine("task completed within timeout");
                    return "OK";
                }
                else
                {
                    cts.Cancel();
                    //Console.WriteLine("timeout logic");
                    return "Invalid IP address or Server not found";
                }
            }
            else
            {
                return "Invalid IP address";
            }
        }

        public void DisconnectFromServer()
        {
            disconnect = true;
            if (client != null)
                client.Close();
            IsConnectedChange(false);
        }

        private async Task OnConnectedTask()
        {
            var taskQueues = concurrentDictionary.Values.ToArray();
            foreach (var queue in taskQueues)
            {
                foreach (var task in queue)
                {
                    //task.SetCanceled();
                    task.SetResult(null);
                }
            }
            concurrentDictionary.Clear();
        }

        //Core
        private async Task ReadThread()
        {
            while (client.Connected)
            {
                var headerBuffer = new byte[MessageHeader.BinarySize];
                var header = new MessageHeader();
                var count = 0;

                try
                {
                    count = await client.GetStream().ReadAsync(headerBuffer, 0, MessageHeader.BinarySize).ConfigureAwait(false);
                    OnRead?.Invoke(true);
                }
                catch (Exception)
                {
                    //Console.WriteLine("Сервер отвалился");
                    OnRead?.Invoke(false);
                    IsConnectedChange(false);
                    //if (disconnect == false)
                    //    Task.Run(() => ConnectToServer(IP));
                }
                if (count != MessageHeader.BinarySize)
                {
                    //fatal error
                    //break;
                    IsConnectedChange(false);
                    OnRead?.Invoke(false);
                    return;
                }

                var b00l = MessageHeader.TryParse(headerBuffer, out header);
                if (b00l == false) Console.WriteLine("AWP: Critical Error: .TryParse");

                switch (header.Code)
                {
                    case 0:
                        ReceiverAddress = header.ReceiverAddress;
                        break;
                    case 1:
                        await HandleResponse<SetActiveListFriendlyUAVsResponse>(header, headerBuffer).ConfigureAwait(false);
                        break;
                    case 2:
                        await HandleResponse<DefaultMessage>(header, headerBuffer).ConfigureAwait(false);
                        break;
                    case 3:
                        await HandleResponse<DefaultMessage>(header, headerBuffer).ConfigureAwait(false);
                        break;
                    case 4:
                        await HandleResponse<DefaultMessage>(header, headerBuffer).ConfigureAwait(false);
                        break;
                    case 5:
                        await HandleResponse<AffiliationUAVResponse>(header, headerBuffer).ConfigureAwait(false);
                        break;

                    default:
                        Console.WriteLine("AWP: Unknown Code: " + header.Code);
                        await ReadResponse<DefaultMessage>(header, headerBuffer).ConfigureAwait(false);
                        break;
                }
            }
        }

        private async Task HandleResponse<T>(MessageHeader header, byte[] headerBuffer) where T : class, IBinarySerializable, new()
        {
            var response = await ReadResponse<T>(header, headerBuffer).ConfigureAwait(false);
            var taskCompletionSource = new TaskCompletionSource<IBinarySerializable>();
            if (!concurrentDictionary[header.Code].TryDequeue(out taskCompletionSource))
            {
                throw new Exception("Response came, but no one is waiting for it");
            }
            if (response != null)
            {
                taskCompletionSource.SetResult(response);
            }
            else
            {
                taskCompletionSource.SetResult(null);
                //taskCompletionSource.SetException(new Exception("Can't execute request"));
            }
        }

        private async Task<T> ReadResponse<T>(MessageHeader header, byte[] headerBuffer) where T : class, IBinarySerializable, new()
        {
            var count = 0;
            var buffer = new byte[MessageHeader.BinarySize + header.InformationLength];
            headerBuffer.CopyTo(buffer, 0);

            if (header.InformationLength != 0)
            {
                var difference = header.InformationLength;

                var shiftpos = 0;

                try
                {
                    while (difference != 0)
                    {
                        count = await client.GetStream().ReadAsync(buffer, MessageHeader.BinarySize + shiftpos, difference).ConfigureAwait(false);
                        if (OnRead != null)
                            OnRead(true);
                        shiftpos += count;
                        difference = difference - count;
                    }
                }
                catch { return null; }
            }

            //if (await client.GetStream().ReadAsync(buffer, MessageHeader.BinarySize, header.InformationLength).ConfigureAwait(false) != header.InformationLength)
            //{
            //    return null;
            //}
            try
            {
                var result = new T();
                result.Decode(buffer, 0);
                return result;
            }
            catch (Exception e)
            {
                var с = e.ToString();
                return null;
            }
        }

        #region Events

        //Info
        public delegate void BytesViewEventHandler(byte cmd, SendReceive SendReceive, byte[] bytes);
        public event BytesViewEventHandler BytesView;

        public enum SendReceive
        {
            Send,
            Receive
        }

        #endregion

        private async Task HandleEvent<T>(MessageHeader header, byte[] headerBuffer) where T : class, IBinarySerializable, new()
        {
            var responseEvent = await ReadResponse<T>(header, headerBuffer).ConfigureAwait(false);

            //Здесь мог бы быть Event


            if (responseEvent == null)
            {
                Console.WriteLine("AWP: responseEvent == null");
            }
        }

        private async Task<TaskCompletionSource<IBinarySerializable>> SendRequest(MessageHeader header, byte[] message)
        {
            using (await asyncLock.LockAsync().ConfigureAwait(false))
            {
                var receiveTask = new TaskCompletionSource<IBinarySerializable>();
                if (!concurrentDictionary.ContainsKey(header.Code))
                {
                    concurrentDictionary[header.Code] = new ConcurrentQueue<TaskCompletionSource<IBinarySerializable>>();
                }
                concurrentDictionary[header.Code].Enqueue(receiveTask);

                int count = message.Count();
                try
                {
                    await client.GetStream().WriteAsync(message, 0, count).ConfigureAwait(false);
                    if (OnWrite != null)
                        OnWrite(true);
                }
                catch (Exception)
                {
                    if (OnWrite != null)
                        OnWrite(false);
                    receiveTask.SetException(new Exception("No connection"));
                }

                return receiveTask;
            }
        }

        //Просто отправить
        private async Task SendRequestMini(MessageHeader header, byte[] message)
        {
            using (await asyncLock.LockAsync().ConfigureAwait(false))
            {
                int count = message.Count();
                try
                {
                    await client.GetStream().WriteAsync(message, 0, count).ConfigureAwait(false);
                    OnWrite?.Invoke(true);
                }
                catch (Exception)
                {
                    OnWrite?.Invoke(false);
                }
            }
        }


        //Шифр 1 
        public async Task<SetActiveListFriendlyUAVsResponse> SetActiveListFriendlyUAVs(IEnumerable<IsActiveUAV> isActiveUAVs)
        {
            if (client == null)
                return null;
            if (client.Connected)
            {
                MessageHeader header = GetMessageHeader(code: 1, length: IsActiveUAV.BinarySize * isActiveUAVs.Count());
                header.SenderAddress = ReceiverAddress;
                header.ReceiverAddress = 0;

                byte[] message = SetActiveListFriendlyUAVsRequest.ToBinary(header, isActiveUAVs.ToArray());
                BytesView?.Invoke(1, SendReceive.Send, message);

                var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
                SetActiveListFriendlyUAVsResponse answer = await taskCompletionSource.Task.ConfigureAwait(false) as SetActiveListFriendlyUAVsResponse;
                BytesView?.Invoke(1, SendReceive.Receive, answer.GetBytes());

                return answer;
            }
            else
            {
                MessageHeader header = new MessageHeader(0, 0, 1, 1, 0);
                SetActiveListFriendlyUAVsResponse answer = new SetActiveListFriendlyUAVsResponse(header, null);
                return answer;
            }
        }

        //Шифр 2
        public async Task<DefaultMessage> AddFriendlyUAV(UAV UAV, byte PreselGain = 0)
        {
            if (client == null)
                return null;
            if (client.Connected)
            {
                MessageHeader header = GetMessageHeader(code: 2, length: AddFriendlyUAVRequest.BinarySize - MessageHeader.BinarySize);
                header.SenderAddress = ReceiverAddress;
                header.ReceiverAddress = 0;

                byte[] message = AddFriendlyUAVRequest.ToBinary(header, UAV, PreselGain);
                BytesView?.Invoke(2, SendReceive.Send, message);

                var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
                DefaultMessage answer = await taskCompletionSource.Task.ConfigureAwait(false) as DefaultMessage;
                BytesView?.Invoke(2, SendReceive.Receive, answer.GetBytes());

                return answer;
            }
            else
            {
                MessageHeader header = new MessageHeader(0, 0, 2, 1, 0);
                DefaultMessage answer = new DefaultMessage(header);
                return answer;
            }
        }

        //Шифр 3
        public async Task<DefaultMessage> DeleteFriendlyUAV(IEnumerable<byte> ID)
        {
            if (client == null)
                return null;
            if (ID.Count() != DeleteFriendlyUAVRequest.IDLength)
            {
                MessageHeader header = new MessageHeader(0, 0, 3, DeleteFriendlyUAVRequest.IDLength, 0);
                DefaultMessage answer = new DefaultMessage(header);
                return answer;
            }
            if (client.Connected)
            {
                MessageHeader header = GetMessageHeader(code: 3, length: DeleteFriendlyUAVRequest.BinarySize - MessageHeader.BinarySize);
                header.SenderAddress = ReceiverAddress;
                header.ReceiverAddress = 0;

                byte[] message = DeleteFriendlyUAVRequest.ToBinary(header, ID.ToArray());
                BytesView?.Invoke(3, SendReceive.Send, message);

                var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
                DefaultMessage answer = await taskCompletionSource.Task.ConfigureAwait(false) as DefaultMessage;
                BytesView?.Invoke(3, SendReceive.Receive, answer.GetBytes());

                return answer;
            }
            else
            {
                MessageHeader header = new MessageHeader(0, 0, 3, 1, 0);
                DefaultMessage answer = new DefaultMessage(header);
                return answer;
            }
        }

        //Шифр 4
        public async Task<DefaultMessage> DeleteFriendlyUAVs()
        {
            if (client == null)
                return null;
            if (client.Connected)
            {
                MessageHeader header = GetMessageHeader(code: 4, length: 0);
                header.SenderAddress = ReceiverAddress;
                header.ReceiverAddress = 0;

                byte[] message = DefaultMessage.ToBinary(header);
                BytesView?.Invoke(4, SendReceive.Send, message);

                var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
                DefaultMessage answer = await taskCompletionSource.Task.ConfigureAwait(false) as DefaultMessage;
                BytesView?.Invoke(4, SendReceive.Receive, answer.GetBytes());

                return answer;
            }
            else
            {
                MessageHeader header = new MessageHeader(0, 0, 4, 1, 0);
                DefaultMessage answer = new DefaultMessage(header);
                return answer;
            }
        }

        //Шифр 5
        public async Task<AffiliationUAVResponse> AffiliationUAV(double ReceiverFrequencyMHz, double SignalCentralFrequencyMHz, float BandwidthMHz, byte PreselGain = 0)
        {
            if (client == null)
                return null;
            if (client.Connected)
            {
                MessageHeader header = GetMessageHeader(code: 5, length: AffiliationUAVRequest.BinarySize - MessageHeader.BinarySize);
                header.SenderAddress = ReceiverAddress;
                header.ReceiverAddress = 0;

                byte[] message = AffiliationUAVRequest.ToBinary(header, ReceiverFrequencyMHz, SignalCentralFrequencyMHz, BandwidthMHz, PreselGain);
                BytesView?.Invoke(5, SendReceive.Send, message);

                var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
                AffiliationUAVResponse answer = await taskCompletionSource.Task.ConfigureAwait(false) as AffiliationUAVResponse;
                BytesView?.Invoke(5, SendReceive.Receive, answer.GetBytes());

                return answer;
            }
            else
            {
                MessageHeader header = new MessageHeader(0, 0, 5, 1, 0);
                AffiliationUAVResponse answer = new AffiliationUAVResponse(header, UAVType.NoSignal, new byte [] { });
                return answer;
            }
        }

    }
}
