﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace CheckClient
{
    public class EndPointConnection : INotifyPropertyChanged, IMethod<EndPointConnection>
    {
        #region NotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged([CallerMemberName] string prop = "")
        {
            try
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
            }
            catch { }
        }

        #endregion

        #region Private

        private string ipAdress;// = "127.0.0.1";
        private int port;

        #endregion

        #region Properties

        public EndPointConnection()
        {
            ipAdress = "127.0.0.1"; //127.0.0.1
            port = 36000;
        }

        public EndPointConnection(int port)
        {
            ipAdress = "127.0.0.1"; //127.0.0.1
            this.port = port;
        }

        [NotifyParentProperty(true)]
        [DisplayName("IP")]
        [Required]
        [RegularExpression(@"\b\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\b", ErrorMessage = "IP : XXX.XXX.XXX.XXX")]
        public string IpAddress
        {
            get { return ipAdress; }
            set
            {
                if (ipAdress == value)
                    return;
                ipAdress = value;
                OnPropertyChanged();
            }
        }

        [NotifyParentProperty(true)]
        //[Required]
        [Range(1, 1000000)]
        public int Port
        {
            get { return port; }
            set
            {
                if (port == value)
                    return;
                port = value;
                OnPropertyChanged();
            }
        }

        #endregion

        public override string ToString()
        {
            return ipAdress + " : " + port;
        }

        #region IMethod

        public EndPointConnection Clone()
        {
            return new EndPointConnection()
            {
                IpAddress = this.ipAdress,
                Port = this.port
            };
        }

        public void Update(EndPointConnection endPoint)
        {
            IpAddress = endPoint.IpAddress;
            Port = endPoint.Port;
        }

        public bool Compare(EndPointConnection data)
        {
            if (ipAdress != data.IpAddress || port != data.Port)
                return false;
            return true;
        }

        #endregion
    }

    public interface IMethod<T> where T : class
    {
        T Clone();
        void Update(T data);
        bool Compare(T data);
    }

    public class YamlUAV
    {
        public byte[] ID { get; set; } = new byte[14];
        public double FreqMinMHz { get; set; }
        public float BandwidthMHz { get; set; }
    }

    public class IsActiveYamlUAV : YamlUAV
    {
        public byte isActive { get; set; }
    }

    public class IsFileSignalYamlUAV : YamlUAV
    {
        public byte isFileSignal { get; set; }
    }

}
