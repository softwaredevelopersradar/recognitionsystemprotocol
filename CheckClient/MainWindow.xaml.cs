﻿using Nito.AsyncEx;
using RecognitionSystemProtocol;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CheckClient
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        RecognitionSystemClient client = new RecognitionSystemClient();

        public void DispatchIfNecessary(Action action)
        {
            if (!Dispatcher.CheckAccess())
                Dispatcher.Invoke(action);
            else
                action.Invoke();
        }

        Yaml yaml = new Yaml();

        public MainWindow()
        {
            InitializeComponent();
            client.OnConnected += Client_IsConnected;

            client.BytesView += Client_BytesView;

            YamlInit();
        }

        private void YamlInit()
        {
            var endPointConnection = yaml.YamlLoad<EndPointConnection>("EndPointConnection.yaml");

            tbIP.Text = endPointConnection.IpAddress;
            tbPort.Text = endPointConnection.Port.ToString();
        }

        private void Client_BytesView(byte cmd, RecognitionSystemClient.SendReceive SendReceive, byte[] bytes)
        {
            DispatchIfNecessary(() =>
            {
                switch (cmd)
                {
                    case 1:
                        switch (SendReceive)
                        {
                            case RecognitionSystemClient.SendReceive.Send:
                                tbCmd1BytesSendView.Content = BytesToStringView(bytes);
                                break;
                            case RecognitionSystemClient.SendReceive.Receive:
                                tbCmd1BytesReceiveView.Content = BytesToStringView(bytes);
                                break;
                        }
                        break;
                    case 2:
                        switch (SendReceive)
                        {
                            case RecognitionSystemClient.SendReceive.Send:
                                tbCmd2BytesSendView.Content = BytesToStringView(bytes);
                                break;
                            case RecognitionSystemClient.SendReceive.Receive:
                                tbCmd2BytesReceiveView.Content = BytesToStringView(bytes);
                                break;
                        }
                        break;
                    case 3:
                        switch (SendReceive)
                        {
                            case RecognitionSystemClient.SendReceive.Send:
                                tbCmd3BytesSendView.Content = BytesToStringView(bytes);
                                break;
                            case RecognitionSystemClient.SendReceive.Receive:
                                tbCmd3BytesReceiveView.Content = BytesToStringView(bytes);
                                break;
                        }
                        break;
                    case 4:
                        switch (SendReceive)
                        {
                            case RecognitionSystemClient.SendReceive.Send:
                                tbCmd4BytesSendView.Content = BytesToStringView(bytes);
                                break;
                            case RecognitionSystemClient.SendReceive.Receive:
                                tbCmd4BytesReceiveView.Content = BytesToStringView(bytes);
                                break;
                        }
                        break;
                    case 5:
                        switch (SendReceive)
                        {
                            case RecognitionSystemClient.SendReceive.Send:
                                tbCmd5BytesSendView.Content = BytesToStringView(bytes);
                                break;
                            case RecognitionSystemClient.SendReceive.Receive:
                                tbCmd5BytesReceiveView.Content = BytesToStringView(bytes);
                                break;
                        }
                        break;
                }
            });
        }

        private string BytesToStringView(IEnumerable<byte> bytes, string separator = " ")
        {
            StringBuilder sb = new StringBuilder();

            for (int i = 0; i < bytes.Count(); i++)
            {
                sb.Append(bytes.ElementAt(i).ToString() + separator);
            }
            return sb.ToString();
        }

        private string BytesToCharAndStringView(IEnumerable<byte> bytes, string separator = " ")
        {
            char[] chars = bytes.Select(x => Convert.ToChar(x)).ToArray();

            StringBuilder sb = new StringBuilder();

            for (int i = 0; i < bytes.Count(); i++)
            {
                sb.Append(chars.ElementAt(i).ToString() + separator);
            }
            return sb.ToString();
        }

        private void Client_IsConnected(bool isConnected)
        {
            DispatchIfNecessary(() =>
            {
                bConnect.Background = (isConnected) ? new SolidColorBrush(Colors.Green) : new SolidColorBrush(Colors.Red);
            });
        }

        private async void bConnect_Click(object sender, RoutedEventArgs e)
        {
            await client.IQConnectToServer(tbIP.Text, Convert.ToInt32(tbPort.Text));
        }

        private void bDisconnect_Click(object sender, RoutedEventArgs e)
        {
            client.DisconnectFromServer();
        }

        private async void bCmd1_Click(object sender, RoutedEventArgs e)
        {
            byte[] ID = CharByteParseID(tbID.Text);
            var Freq = TryCustomParseDouble(tbFreq.Text);
            var BandWidth = TryCustomParseDouble(tbBandWidth.Text);
            var isActive = Convert.ToByte(tbIsActive.Text);

            UAV uav = new UAV() { ID = ID, FreqMinMHz = Freq.result, BandwidthMHz = (float)BandWidth.result };
            IsActiveUAV isActiveUAV = new IsActiveUAV() { ActiveUAV = uav, isActive = isActive };
            IsActiveUAV[] Set = new IsActiveUAV[] { isActiveUAV };

            SetActiveListFriendlyUAVsResponse answer = null;
            if (Freq.flag && BandWidth.flag)
            {
                answer = await client.SetActiveListFriendlyUAVs(Set);
            }
            if (answer != null)
            {
                DispatchIfNecessary(() =>
                {
                    tbID11.Text = BytesToCharAndStringView(answer.IsFileSignalUAVs[0].FileSignalUAV.ID, "");
                    tbFreq11.Text = answer.IsFileSignalUAVs[0].FileSignalUAV.FreqMinMHz.ToString();
                    tbBandWidth11.Text = answer.IsFileSignalUAVs[0].FileSignalUAV.BandwidthMHz.ToString();
                    tbIsActive11.Text = answer.IsFileSignalUAVs[0].isFileSignal.ToString();
                });
            }
        }

        private async void bCmd1File_Click(object sender, RoutedEventArgs e)
        {
            //List<IsActiveYamlUAV> isActiveYamlUAVs = new List<IsActiveYamlUAV>();

            //isActiveYamlUAVs.Add(new IsActiveYamlUAV() { FreqMinMHz = 1, BandwidthMHz = 2, isActive = 0 });
            //isActiveYamlUAVs.Add(new IsActiveYamlUAV() { FreqMinMHz = 3, BandwidthMHz = 4, isActive = 1 });
            //isActiveYamlUAVs.Add(new IsActiveYamlUAV() { FreqMinMHz = 5, BandwidthMHz = 6, isActive = 0 });

            //yaml.YamlSave<List<IsActiveYamlUAV>>(isActiveYamlUAVs, "Cmd1Input.yaml");

            var ActiveYamlUAVs = yaml.YamlLoad<List<IsActiveYamlUAV>>("Cmd1Input.yaml");

            List<IsActiveUAV> isActiveUAVs = new List<IsActiveUAV>();

            foreach (IsActiveYamlUAV isActiveYamlUAV in ActiveYamlUAVs)
            {
                UAV uav = new UAV() { ID = isActiveYamlUAV.ID, FreqMinMHz = isActiveYamlUAV.FreqMinMHz, BandwidthMHz = isActiveYamlUAV.BandwidthMHz };
                IsActiveUAV isActiveUAV = new IsActiveUAV() { ActiveUAV = uav, isActive = isActiveYamlUAV.isActive };
                isActiveUAVs.Add(isActiveUAV);
            }

            var answer = await client.SetActiveListFriendlyUAVs(isActiveUAVs);

            yaml.YamlSave(answer.IsFileSignalUAVs.ToList(), "Cmd1Output.yaml");
        }

        private async void bCmd2_Click(object sender, RoutedEventArgs e)
        {
            byte[] ID = CharByteParseID(tbID2.Text);
            var Freq = TryCustomParseDouble(tbFreq2.Text);
            var BandWidth = TryCustomParseDouble(tbBandWidth2.Text);
            var PreselGain = Convert.ToByte(tbPreselGain2.Text);

            UAV uav = new UAV() { ID = ID, FreqMinMHz = Freq.result, BandwidthMHz = (float)BandWidth.result };

            DefaultMessage answer = null;
            if (Freq.flag && BandWidth.flag)
            {
                answer = await client.AddFriendlyUAV(uav, PreselGain);
            }
        }

        private async void bCmd3_Click(object sender, RoutedEventArgs e)
        {
            byte[] ID = CharByteParseID(tbID3.Text);

            DefaultMessage answer = await client.DeleteFriendlyUAV(ID);
        }

        private async void bCmd4_Click(object sender, RoutedEventArgs e)
        {
            DefaultMessage answer = await client.DeleteFriendlyUAVs();
        }

        private async void bCmd5_Click(object sender, RoutedEventArgs e)
        {
            var RecFreq = TryCustomParseDouble(tbRecFreq5.Text);
            var Freq = TryCustomParseDouble(tbFreq5.Text);
            var BandWidth = TryCustomParseDouble(tbBandWidth5.Text);
            var PreselGain = Convert.ToByte(tbPreselGain2.Text);

            AffiliationUAVResponse answer = null;
            if (RecFreq.flag && Freq.flag && BandWidth.flag)
            {
                answer = await client.AffiliationUAV(RecFreq.result, Freq.result, (float)BandWidth.result, PreselGain);
            }
            if (answer != null)
            {
                DispatchIfNecessary(() =>
                {
                    Type55.Text = ((int)answer.Type).ToString();
                    tbID55.Text = BytesToCharAndStringView(answer.ID, "");
                });
            }
        }

        private string SaveString(string str)
        {
            return str.Replace(".", ",");
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            string str1 = "23.45";
            string str2 = "23,45";


            var a1 = TryCustomParseDouble(str1);
            var a2 = TryCustomParseDouble(str2);
        }

        private (bool flag, double result) TryCustomParseDouble(string value)
        {
            bool flag = false;
            double result = 0;

            flag = double.TryParse(value, out result);

            if (!flag)
            {
                if (value.Contains("."))
                {
                    value = value.Replace(".", ",");
                    flag = double.TryParse(value, out result);
                    return (flag, result);
                }
                if (value.Contains(","))
                {
                    value = value.Replace(",", ".");
                    flag = double.TryParse(value, out result);
                    return (flag, result);
                }
            }

            return (flag, result);
        }


        private byte[] TryCustomParseID(string value)
        {
            var charArray = value.ToCharArray();

            var bytes = charArray.Select(x => Byte.Parse(x.ToString())).ToArray();

            return bytes;
        }

        private byte[] CharByteParseID(string value)
        {
            var charArray = value.ToCharArray();

            var bytes = charArray.Select(x => Convert.ToByte(x)).ToArray();

            return bytes;
        }

        bool flagCircle = false;

        private readonly AsyncAutoResetEvent _AsyncAutoResetEvent = new AsyncAutoResetEvent();

        private async void Start_Click(object sender, RoutedEventArgs e)
        {
            lCircle.Foreground = new SolidColorBrush(Colors.Green);

            flagCircle = true;
            Random random = new Random();

            while (flagCircle)
            {
                double ReceiverFrequencyMHz = random.Next(2000, 6000);
                short ID = (short)random.Next(0, 32000);

                double coef = (random.Next(0, 2) == 0) ? -1 : 1;

                double FrequencyMHz = ReceiverFrequencyMHz + random.Next(5, 20) * coef;

                float BandwidthMHz = random.Next(5, 20);

                byte TypeTable = (byte)random.Next(0, 100);

                //tbRecFreq.Text = ReceiverFrequencyMHz.ToString();
                //tbID.Text = ID.ToString();
                //tbFreq.Text = FrequencyMHz.ToString();
                //tbBandWidth.Text = BandwidthMHz.ToString();
                //tbTypeTable.Text = TypeTable.ToString();

                //var answer = await client.SetFrequency(ReceiverFrequencyMHz, ID, FrequencyMHz, BandwidthMHz, TypeTable);

                await _AsyncAutoResetEvent.WaitAsync();
                await Task.Delay(1);
            }
        }

        private void Stop_Click(object sender, RoutedEventArgs e)
        {
            lCircle.Foreground = new SolidColorBrush(Colors.Red);

            flagCircle = false;
            _AsyncAutoResetEvent.Set();
        }


    }
}
