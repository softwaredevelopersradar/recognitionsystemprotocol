﻿using llcss;
using Nito.AsyncEx;
using RecognitionSystemProtocol2;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace RecognitionSystemProtocol2
{
    public class RecognitionSystemClient2
    {
        private TcpClient client;
        private string IP;
        private int port = 36000;

        private byte ReceiverAddress;

        private readonly AsyncLock asyncLock;

        private readonly ConcurrentDictionary<int, ConcurrentQueue<TaskCompletionSource<IBinarySerializable>>> concurrentDictionary;

        private bool disconnect = false;

        private bool _IsConnected = false;
        public bool IsConnected
        {
            get { return _IsConnected; }
            set
            {
                _IsConnected = value;
                OnConnected?.Invoke(value);
            }
        }

        private void IsConnectedChange(bool value)
        {
            IsConnected = value;
        }

        public delegate void OnConnectedEventHandler(bool onConnected);
        public event OnConnectedEventHandler OnConnected;

        public delegate void OnReadEventHandler(bool onRead);
        public event OnReadEventHandler OnRead;

        public delegate void OnWriteEventHandler(bool onWrite);
        public event OnWriteEventHandler OnWrite;

        public RecognitionSystemClient2()
        {
            asyncLock = new AsyncLock();
            concurrentDictionary = new ConcurrentDictionary<int, ConcurrentQueue<TaskCompletionSource<IBinarySerializable>>>();
        }

        static byte OwnServerAddress = 0;
        static MessageHeader GetMessageHeader(int code, int length)
        {
            //return new MessageHeader(0, 1, (byte)code, 0, length);
            return new MessageHeader(OwnServerAddress, 1, (byte)code, 0, length);
        }

        private async Task ConnectToServer2(string IP, int port, CancellationToken token)
        {
            disconnect = false;
            client = new TcpClient();

            while (!client.Connected)
            {
                if (token.IsCancellationRequested)
                    return;
                try
                {
                    await client.ConnectAsync(IPAddress.Parse(IP), port).ConfigureAwait(false);

                    this.IP = IP;
                    this.port = port;
                }
                catch (Exception)
                {

                }
                if (!client.Connected)
                    await Task.Delay(500);
            }

            Console.WriteLine("ConnectRecognitionSystem2");

            await OnConnectedTask();

            Task.Run(() => ReadThread());

            IsConnectedChange(true);
        }

        public async Task<string> IQConnectToServer(string IP, int port, int timeout = 3000)
        {
            Regex regex = new Regex(@"^(25[0-5]|2[0-4][0-9]|[0-1][0-9]{2}|[0-9]{2}|[0-9])(\.(25[0-5]|2[0-4][0-9]|[0-1][0-9]{2}|[0-9]{2}|[0-9])){3}$");
            if (regex.IsMatch(IP, 0))
            {
                CancellationTokenSource cts = new CancellationTokenSource();
                CancellationToken token = cts.Token;

                var task = ConnectToServer2(IP, port, token);

                if (await Task.WhenAny(task, Task.Delay(timeout)) == task)
                {
                    //Console.WriteLine("task completed within timeout");
                    return "OK";
                }
                else
                {
                    cts.Cancel();
                    //Console.WriteLine("timeout logic");
                    return "Invalid IP address or Server not found";
                }
            }
            else
            {
                return "Invalid IP address";
            }
        }

        public void DisconnectFromServer()
        {
            disconnect = true;
            if (client != null)
                client.Close();
            IsConnectedChange(false);
        }

        private async Task OnConnectedTask()
        {
            var taskQueues = concurrentDictionary.Values.ToArray();
            foreach (var queue in taskQueues)
            {
                foreach (var task in queue)
                {
                    //task.SetCanceled();
                    task.SetResult(null);
                }
            }
            concurrentDictionary.Clear();
        }

        //Core
        private async Task ReadThread()
        {
            while (client.Connected)
            {
                var headerBuffer = new byte[MessageHeader.BinarySize];
                var header = new MessageHeader();
                var count = 0;

                try
                {
                    count = await client.GetStream().ReadAsync(headerBuffer, 0, MessageHeader.BinarySize).ConfigureAwait(false);
                    OnRead?.Invoke(true);
                }
                catch (Exception)
                {
                    //Console.WriteLine("Сервер отвалился");
                    OnRead?.Invoke(false);
                    IsConnectedChange(false);
                    //if (disconnect == false)
                    //    Task.Run(() => ConnectToServer(IP));
                }
                if (count != MessageHeader.BinarySize)
                {
                    //fatal error
                    //break;
                    IsConnectedChange(false);
                    OnRead?.Invoke(false);
                    return;
                }

                var b00l = MessageHeader.TryParse(headerBuffer, out header);
                if (b00l == false) Console.WriteLine("AWP: Critical Error: .TryParse");

                switch (header.Code)
                {
                    case 0:
                        ReceiverAddress = header.ReceiverAddress;
                        break;
                    case 1:
                        await HandleResponse<GetActiveListFriendlyUAVsResponse>(header, headerBuffer).ConfigureAwait(false);
                        break;
                    case 2:
                        await HandleResponse<GetActiveListFriendlyUAVsResponse>(header, headerBuffer).ConfigureAwait(false);
                        break;
                    case 3:
                        await HandleResponse<GetActiveListFriendlyUAVsResponse>(header, headerBuffer).ConfigureAwait(false);
                        break;
                    case 4:
                        await HandleResponse<GetActiveListFriendlyUAVsResponse>(header, headerBuffer).ConfigureAwait(false);
                        break;
                    case 5:
                        await HandleResponse<GetActiveListFriendlyUAVsResponse>(header, headerBuffer).ConfigureAwait(false);
                        break;
                    case 6:
                        await HandleResponse<DefaultMessage>(header, headerBuffer).ConfigureAwait(false);
                        break;
                    case 7:
                        await HandleResponse<DefaultMessage>(header, headerBuffer).ConfigureAwait(false);
                        break;
                    //case 6:
                    //    await HandleResponse<GetActiveListFriendlyUAVsResponse>(header, headerBuffer).ConfigureAwait(false);
                    //    break;
                    //case 7:
                    //    await HandleResponse<AffiliationUAVResponse>(header, headerBuffer).ConfigureAwait(false);
                    //    break;
                    case 8:
                        await HandleResponse<GetFFTResponce>(header, headerBuffer).ConfigureAwait(false);
                        break;
                    case 9:
                        await HandleResponse<DefaultMessage>(header, headerBuffer).ConfigureAwait(false);
                        break;
                    case 10:
                        await HandleResponse<DefaultMessage>(header, headerBuffer).ConfigureAwait(false);
                        break;
                    case 11:
                        await HandleResponse<DefaultMessage>(header, headerBuffer).ConfigureAwait(false);
                        break;
                    case 12:
                        await HandleResponse<GetNewSignalsResponse>(header, headerBuffer).ConfigureAwait(false);
                        break;
                    case 13:
                        await HandleResponse<DefaultMessage>(header, headerBuffer).ConfigureAwait(false);
                        break;
                    case 14:
                        await HandleEvent<GetNewSignalsResponse>(header, headerBuffer).ConfigureAwait(false);
                        break;
                    case 15:
                        await HandleEvent<AutoRiAndRecResponce>(header, headerBuffer).ConfigureAwait(false);
                        break;
                    case 16:
                        await HandleResponse<DefaultMessage>(header, headerBuffer).ConfigureAwait(false);
                        break;
                    default:
                        Console.WriteLine("AWP: Unknown Code: " + header.Code);
                        await ReadResponse<DefaultMessage>(header, headerBuffer).ConfigureAwait(false);
                        break;
                }
            }
        }

        private async Task HandleResponse<T>(MessageHeader header, byte[] headerBuffer) where T : class, IBinarySerializable, new()
        {
            var response = await ReadResponse<T>(header, headerBuffer).ConfigureAwait(false);
            var taskCompletionSource = new TaskCompletionSource<IBinarySerializable>();
            if (!concurrentDictionary[header.Code].TryDequeue(out taskCompletionSource))
            {
                throw new Exception("Response came, but no one is waiting for it");
            }
            if (response != null)
            {
                taskCompletionSource.SetResult(response);
            }
            else
            {
                taskCompletionSource.SetResult(null);
                //taskCompletionSource.SetException(new Exception("Can't execute request"));
            }
        }

        private async Task<T> ReadResponse<T>(MessageHeader header, byte[] headerBuffer) where T : class, IBinarySerializable, new()
        {
            var count = 0;
            var buffer = new byte[MessageHeader.BinarySize + header.InformationLength];
            headerBuffer.CopyTo(buffer, 0);

            if (header.InformationLength != 0)
            {
                var difference = header.InformationLength;

                var shiftpos = 0;

                try
                {
                    while (difference != 0)
                    {
                        count = await client.GetStream().ReadAsync(buffer, MessageHeader.BinarySize + shiftpos, difference).ConfigureAwait(false);
                        if (OnRead != null)
                            OnRead(true);
                        shiftpos += count;
                        difference = difference - count;
                    }
                }
                catch { return null; }
            }

            //if (await client.GetStream().ReadAsync(buffer, MessageHeader.BinarySize, header.InformationLength).ConfigureAwait(false) != header.InformationLength)
            //{
            //    return null;
            //}
            try
            {
                var result = new T();
                result.Decode(buffer, 0);
                return result;
            }
            catch (IndexOutOfRangeException e)
            {
                var result = new T();

                var bytes = result.GetBytes();
                var bytesHeader = header.GetBytes();

                for (int i = 0; i < bytesHeader.Length; i++)
                {
                    bytes[i] = bytesHeader[i];
                }

                result.Decode(bytes, 0);
                return result;
            }
            catch (Exception e)
            {
                var с = e.ToString();
                return null;
            }
        }

        #region Events

        //Info
        public delegate void BytesViewEventHandler(byte cmd, SendReceive SendReceive, byte[] bytes);
        public event BytesViewEventHandler BytesView;

        //Шифр 14
        public delegate void GetNewSignalsEventHandler(GetNewSignalsResponse answer);
        public event GetNewSignalsEventHandler GetNewSignalsUpdate;

        //Шифр 15
        public delegate void AutoRiAndRecEventHandler(AutoRiAndRecResponce answer);
        public event AutoRiAndRecEventHandler AutoRiAndRecUpdate;

        public enum SendReceive
        {
            Send,
            Receive
        }

        #endregion

        private async Task HandleEvent<T>(MessageHeader header, byte[] headerBuffer) where T : class, IBinarySerializable, new()
        {
            var responseEvent = await ReadResponse<T>(header, headerBuffer).ConfigureAwait(false);

            //Здесь мог бы быть Event

            //Шифр 14
            if (responseEvent is GetNewSignalsResponse)
            {
                var answer = responseEvent as GetNewSignalsResponse;
                switch (answer?.Header.Code)
                {
                    case 14:
                        GetNewSignalsUpdate?.Invoke(answer);
                        break;
                }
            }

            //Шифр 15
            if (responseEvent is AutoRiAndRecResponce)
            {
                var answer = responseEvent as AutoRiAndRecResponce;
                switch (answer?.Header.Code)
                {
                    case 15:
                        AutoRiAndRecUpdate?.Invoke(answer);
                        break;
                }
            }

            if (responseEvent == null)
            {
                Console.WriteLine("AWP: responseEvent == null");
            }
        }

        private async Task<TaskCompletionSource<IBinarySerializable>> SendRequest(MessageHeader header, byte[] message)
        {
            using (await asyncLock.LockAsync().ConfigureAwait(false))
            {
                var receiveTask = new TaskCompletionSource<IBinarySerializable>();
                if (!concurrentDictionary.ContainsKey(header.Code))
                {
                    concurrentDictionary[header.Code] = new ConcurrentQueue<TaskCompletionSource<IBinarySerializable>>();
                }
                concurrentDictionary[header.Code].Enqueue(receiveTask);

                int count = message.Count();
                try
                {
                    await client.GetStream().WriteAsync(message, 0, count).ConfigureAwait(false);
                    if (OnWrite != null)
                        OnWrite(true);
                }
                catch (Exception)
                {
                    if (OnWrite != null)
                        OnWrite(false);
                    receiveTask.SetException(new Exception("No connection"));
                }

                return receiveTask;
            }
        }

        //Просто отправить
        private async Task SendRequestMini(MessageHeader header, byte[] message)
        {
            using (await asyncLock.LockAsync().ConfigureAwait(false))
            {
                int count = message.Count();
                try
                {
                    await client.GetStream().WriteAsync(message, 0, count).ConfigureAwait(false);
                    OnWrite?.Invoke(true);
                }
                catch (Exception)
                {
                    OnWrite?.Invoke(false);
                }
            }
        }


        //Шифр 1 
        public async Task<GetActiveListFriendlyUAVsResponse> GetActiveListFriendlyUAVs()
        {
            if (client == null)
                return null;
            if (client.Connected)
            {
                MessageHeader header = GetMessageHeader(code: 1, length: 0);
                header.SenderAddress = ReceiverAddress;
                header.ReceiverAddress = 0;

                byte[] message = DefaultMessage.ToBinary(header);
                BytesView?.Invoke(1, SendReceive.Send, message);

                var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
                GetActiveListFriendlyUAVsResponse answer = await taskCompletionSource.Task.ConfigureAwait(false) as GetActiveListFriendlyUAVsResponse;
                BytesView?.Invoke(1, SendReceive.Receive, answer.GetBytes());

                return answer;
            }
            else
            {
                MessageHeader header = new MessageHeader(0, 0, 1, 1, 0);
                GetActiveListFriendlyUAVsResponse answer = new GetActiveListFriendlyUAVsResponse(header, 0, null);
                return answer;
            }
        }
        public async Task<GetActiveListFriendlyUAVsStringResponse> GetActiveListFriendlyUAVsString()
        {
            if (client == null)
                return null;
            if (client.Connected)
            {
                MessageHeader header = GetMessageHeader(code: 1, length: 0);
                header.SenderAddress = ReceiverAddress;
                header.ReceiverAddress = 0;

                byte[] message = DefaultMessage.ToBinary(header);
                BytesView?.Invoke(1, SendReceive.Send, message);

                var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
                GetActiveListFriendlyUAVsResponse answer = await taskCompletionSource.Task.ConfigureAwait(false) as GetActiveListFriendlyUAVsResponse;
                BytesView?.Invoke(1, SendReceive.Receive, answer.GetBytes());

                return answer.ToGetActiveListFriendlyUAVsStringResponse();
            }
            else
            {
                MessageHeader header = new MessageHeader(0, 0, 1, 1, 0);
                GetActiveListFriendlyUAVsResponse answer = new GetActiveListFriendlyUAVsResponse(header, 0, null);
                return answer.ToGetActiveListFriendlyUAVsStringResponse();
            }
        }

        //Шифр 2
        public async Task<GetActiveListFriendlyUAVsResponse> AddFriendlyUAVs(UAVandText uav, IEnumerable<Active> Actives)
        {
            if (client == null)
                return null;
            if (client.Connected)
            {
                MessageHeader header = GetMessageHeader(code: 2, length: UAVandText.BinarySize + Active.BinarySize * Actives.Count());
                header.SenderAddress = ReceiverAddress;
                header.ReceiverAddress = 0;

                byte[] message = SetFriendlyUAVsRequest.ToBinary(header, uav, Actives.ToArray());
                BytesView?.Invoke(2, SendReceive.Send, message);

                var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
                GetActiveListFriendlyUAVsResponse answer = await taskCompletionSource.Task.ConfigureAwait(false) as GetActiveListFriendlyUAVsResponse;
                BytesView?.Invoke(2, SendReceive.Receive, answer.GetBytes());

                return answer;
            }
            else
            {
                MessageHeader header = new MessageHeader(0, 0, 2, 1, 0);
                GetActiveListFriendlyUAVsResponse answer = new GetActiveListFriendlyUAVsResponse(header, 0, null);
                return answer;
            }
        }
        public async Task<GetActiveListFriendlyUAVsStringResponse> AddFriendlyUAVs(UAVandString uav, IEnumerable<Active> Actives)
        {
            if (client == null)
                return null;
            if (client.Connected)
            {
                MessageHeader header = GetMessageHeader(code: 2, length: UAVandText.BinarySize + Active.BinarySize * Actives.Count());
                header.SenderAddress = ReceiverAddress;
                header.ReceiverAddress = 0;

                byte[] message = SetFriendlyUAVsRequest.ToBinary(header, uav.ToUAVandText(), Actives.ToArray());
                BytesView?.Invoke(2, SendReceive.Send, message);

                var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
                GetActiveListFriendlyUAVsResponse answer = await taskCompletionSource.Task.ConfigureAwait(false) as GetActiveListFriendlyUAVsResponse;
                BytesView?.Invoke(2, SendReceive.Receive, answer.GetBytes());

                return answer.ToGetActiveListFriendlyUAVsStringResponse();
            }
            else
            {
                MessageHeader header = new MessageHeader(0, 0, 2, 1, 0);
                GetActiveListFriendlyUAVsResponse answer = new GetActiveListFriendlyUAVsResponse(header, 0, null);
                return answer.ToGetActiveListFriendlyUAVsStringResponse();
            }
        }

        //Шифр 3
        public async Task<GetActiveListFriendlyUAVsResponse> SetFriendlyUAVs(UAVandText uav, IEnumerable<Active> Actives)
        {
            if (client == null)
                return null;
            if (client.Connected)
            {
                MessageHeader header = GetMessageHeader(code: 3, length: UAVandText.BinarySize + Active.BinarySize * Actives.Count());
                header.SenderAddress = ReceiverAddress;
                header.ReceiverAddress = 0;

                byte[] message = SetFriendlyUAVsRequest.ToBinary(header, uav, Actives.ToArray());
                BytesView?.Invoke(3, SendReceive.Send, message);

                var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
                GetActiveListFriendlyUAVsResponse answer = await taskCompletionSource.Task.ConfigureAwait(false) as GetActiveListFriendlyUAVsResponse;
                BytesView?.Invoke(3, SendReceive.Receive, answer.GetBytes());

                return answer;
            }
            else
            {
                MessageHeader header = new MessageHeader(0, 0, 3, 1, 0);
                GetActiveListFriendlyUAVsResponse answer = new GetActiveListFriendlyUAVsResponse(header, 0, null);
                return answer;
            }
        }
        public async Task<GetActiveListFriendlyUAVsStringResponse> SetFriendlyUAVs(UAVandString uav, IEnumerable<Active> Actives)
        {
            if (client == null)
                return null;
            if (client.Connected)
            {
                MessageHeader header = GetMessageHeader(code: 3, length: UAVandText.BinarySize + Active.BinarySize * Actives.Count());
                header.SenderAddress = ReceiverAddress;
                header.ReceiverAddress = 0;

                byte[] message = SetFriendlyUAVsRequest.ToBinary(header, uav.ToUAVandText(), Actives.ToArray());
                BytesView?.Invoke(3, SendReceive.Send, message);

                var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
                GetActiveListFriendlyUAVsResponse answer = await taskCompletionSource.Task.ConfigureAwait(false) as GetActiveListFriendlyUAVsResponse;
                BytesView?.Invoke(3, SendReceive.Receive, answer.GetBytes());

                return answer.ToGetActiveListFriendlyUAVsStringResponse();
            }
            else
            {
                MessageHeader header = new MessageHeader(0, 0, 3, 1, 0);
                GetActiveListFriendlyUAVsResponse answer = new GetActiveListFriendlyUAVsResponse(header, 0, null);
                return answer.ToGetActiveListFriendlyUAVsStringResponse();
            }
        }

        //Шифр 4
        public async Task<GetActiveListFriendlyUAVsResponse> DeleteFriendlyUAV(IEnumerable<byte> ID)
        {
            if (client == null)
                return null;
            if (ID.Count() != DeleteFriendlyUAVRequest.IDLength)
            {
                MessageHeader header = new MessageHeader(0, 0, 4, 2, 0);
                GetActiveListFriendlyUAVsResponse answer = new GetActiveListFriendlyUAVsResponse(header, 0, null);
                return answer;
            }
            if (client.Connected)
            {
                MessageHeader header = GetMessageHeader(code: 4, length: DeleteFriendlyUAVRequest.BinarySize - MessageHeader.BinarySize);
                header.SenderAddress = ReceiverAddress;
                header.ReceiverAddress = 0;

                byte[] message = DeleteFriendlyUAVRequest.ToBinary(header, ID.ToArray());
                BytesView?.Invoke(4, SendReceive.Send, message);

                var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
                GetActiveListFriendlyUAVsResponse answer = await taskCompletionSource.Task.ConfigureAwait(false) as GetActiveListFriendlyUAVsResponse;
                BytesView?.Invoke(4, SendReceive.Receive, answer.GetBytes());

                return answer;
            }
            else
            {
                MessageHeader header = new MessageHeader(0, 0, 4, 1, 0);
                GetActiveListFriendlyUAVsResponse answer = new GetActiveListFriendlyUAVsResponse(header,0, null);
                return answer;
            }
        }
        public async Task<GetActiveListFriendlyUAVsStringResponse> DeleteFriendlyUAV(string ID)
        {
            if (client == null)
                return null;
            if (client.Connected)
            {
                MessageHeader header = GetMessageHeader(code: 4, length: DeleteFriendlyUAVRequest.BinarySize - MessageHeader.BinarySize);
                header.SenderAddress = ReceiverAddress;
                header.ReceiverAddress = 0;

                byte[] message = DeleteFriendlyUAVRequest.ToBinary(header, Support.StringToCharBytes(ID, DeleteFriendlyUAVRequest.IDLength));
                BytesView?.Invoke(4, SendReceive.Send, message);

                var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
                GetActiveListFriendlyUAVsResponse answer = await taskCompletionSource.Task.ConfigureAwait(false) as GetActiveListFriendlyUAVsResponse;
                BytesView?.Invoke(4, SendReceive.Receive, answer.GetBytes());

                return answer.ToGetActiveListFriendlyUAVsStringResponse();
            }
            else
            {
                MessageHeader header = new MessageHeader(0, 0, 4, 1, 0);
                GetActiveListFriendlyUAVsResponse answer = new GetActiveListFriendlyUAVsResponse(header, 0, null);
                return answer.ToGetActiveListFriendlyUAVsStringResponse();
            }
        }

        //Шифр 5
        public async Task<GetActiveListFriendlyUAVsResponse> DeleteFriendlyUAVs()
        {
            if (client == null)
                return null;
            if (client.Connected)
            {
                MessageHeader header = GetMessageHeader(code: 5, length: 0);
                header.SenderAddress = ReceiverAddress;
                header.ReceiverAddress = 0;

                byte[] message = DefaultMessage.ToBinary(header);
                BytesView?.Invoke(5, SendReceive.Send, message);

                var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
                GetActiveListFriendlyUAVsResponse answer = await taskCompletionSource.Task.ConfigureAwait(false) as GetActiveListFriendlyUAVsResponse;
                BytesView?.Invoke(5, SendReceive.Receive, answer.GetBytes());

                return answer;
            }
            else
            {
                MessageHeader header = new MessageHeader(0, 0, 5, 1, 0);
                GetActiveListFriendlyUAVsResponse answer = new GetActiveListFriendlyUAVsResponse(header, 0, null);
                return answer;
            }
        }
        public async Task<GetActiveListFriendlyUAVsStringResponse> DeleteFriendlyUAVsString()
        {
            if (client == null)
                return null;
            if (client.Connected)
            {
                MessageHeader header = GetMessageHeader(code: 5, length: 0);
                header.SenderAddress = ReceiverAddress;
                header.ReceiverAddress = 0;

                byte[] message = DefaultMessage.ToBinary(header);
                BytesView?.Invoke(5, SendReceive.Send, message);

                var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
                GetActiveListFriendlyUAVsResponse answer = await taskCompletionSource.Task.ConfigureAwait(false) as GetActiveListFriendlyUAVsResponse;
                BytesView?.Invoke(5, SendReceive.Receive, answer.GetBytes());

                return answer.ToGetActiveListFriendlyUAVsStringResponse();
            }
            else
            {
                MessageHeader header = new MessageHeader(0, 0, 5, 1, 0);
                GetActiveListFriendlyUAVsResponse answer = new GetActiveListFriendlyUAVsResponse(header, 0, null);
                return answer.ToGetActiveListFriendlyUAVsStringResponse();
            }
        }

        //Шифр 6
        public async Task<GetActiveListFriendlyUAVsResponse> RecordSignalUAV(isGainUAV RecordSignalUAV)
        {
            if (client == null)
                return null;
            if (client.Connected)
            {
                MessageHeader header = GetMessageHeader(code: 6, length: isGainUAV.BinarySize);
                header.SenderAddress = ReceiverAddress;
                header.ReceiverAddress = 0;

                byte[] message = RecordSignalUAVRequest.ToBinary(header, RecordSignalUAV);
                BytesView?.Invoke(6, SendReceive.Send, message);

                var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
                GetActiveListFriendlyUAVsResponse answer = await taskCompletionSource.Task.ConfigureAwait(false) as GetActiveListFriendlyUAVsResponse;
                BytesView?.Invoke(6, SendReceive.Receive, answer.GetBytes());

                return answer;
            }
            else
            {
                MessageHeader header = new MessageHeader(0, 0, 6, 1, 0);
                GetActiveListFriendlyUAVsResponse answer = new GetActiveListFriendlyUAVsResponse(header, 0, null);
                return answer;
            }
        }

        //Шифр 6 для grpc
        public async Task<DefaultMessage> RecordSignalUAV(int idOwnUAVFreq)
        {
            if (client == null)
                return null;
            if (client.Connected)
            {
                MessageHeader header = GetMessageHeader(code: 6, length: IdRequest.BinarySize - MessageHeader.BinarySize);
                header.SenderAddress = ReceiverAddress;
                header.ReceiverAddress = 0;

                byte[] message = IdRequest.ToBinary(header, idOwnUAVFreq);
                BytesView?.Invoke(6, SendReceive.Send, message);

                var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
                DefaultMessage answer = await taskCompletionSource.Task.ConfigureAwait(false) as DefaultMessage;
                BytesView?.Invoke(6, SendReceive.Receive, answer.GetBytes());

                return answer;
            }
            else
            {
                MessageHeader header = new MessageHeader(0, 0, 6, 1, 0);
                DefaultMessage answer = new DefaultMessage(header);
                return answer;
            }
        }

        public async Task<GetActiveListFriendlyUAVsStringResponse> RecordSignalUAVString(isGainUAV RecordSignalUAV)
        {
            if (client == null)
                return null;
            if (client.Connected)
            {
                MessageHeader header = GetMessageHeader(code: 6, length: isGainUAV.BinarySize);
                header.SenderAddress = ReceiverAddress;
                header.ReceiverAddress = 0;

                byte[] message = RecordSignalUAVRequest.ToBinary(header, RecordSignalUAV);
                BytesView?.Invoke(6, SendReceive.Send, message);

                var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
                GetActiveListFriendlyUAVsResponse answer = await taskCompletionSource.Task.ConfigureAwait(false) as GetActiveListFriendlyUAVsResponse;
                BytesView?.Invoke(6, SendReceive.Receive, answer.GetBytes());

                return answer.ToGetActiveListFriendlyUAVsStringResponse();
            }
            else
            {
                MessageHeader header = new MessageHeader(0, 0, 6, 1, 0);
                GetActiveListFriendlyUAVsResponse answer = new GetActiveListFriendlyUAVsResponse(header, 0, null);
                return answer.ToGetActiveListFriendlyUAVsStringResponse();
            }
        }

        //Шифр 7
        public async Task<AffiliationUAVResponse> AffiliationUAV(int RadioSourceID, double FreqMinMHz, float BandwidthMHz, byte ValuePreselGain, byte ValueAntGain)
        {
            if (client == null)
                return null;
            if (client.Connected)
            {
                MessageHeader header = GetMessageHeader(code: 7, length: AffiliationUAVRequest.BinarySize - MessageHeader.BinarySize);
                header.SenderAddress = ReceiverAddress;
                header.ReceiverAddress = 0;

                byte[] message = AffiliationUAVRequest.ToBinary(header, RadioSourceID, FreqMinMHz, BandwidthMHz, ValuePreselGain, ValueAntGain);
                BytesView?.Invoke(7, SendReceive.Send, message);

                var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
                AffiliationUAVResponse answer = await taskCompletionSource.Task.ConfigureAwait(false) as AffiliationUAVResponse;
                BytesView?.Invoke(7, SendReceive.Receive, answer.GetBytes());

                return answer;
            }
            else
            {
                MessageHeader header = new MessageHeader(0, 0, 7, 1, 0);
                AffiliationUAVResponse answer = new AffiliationUAVResponse(header, 0, UAVType.NoSignal, new byte[] { });
                return answer;
            }
        }

        //Шифр 7 для grpc
        public async Task<DefaultMessage> AffiliationUAV(int idSource)
        {
            if (client == null)
                return null;
            if (client.Connected)
            {
                MessageHeader header = GetMessageHeader(code: 7, length: IdRequest.BinarySize - MessageHeader.BinarySize);
                header.SenderAddress = ReceiverAddress;
                header.ReceiverAddress = 0;

                byte[] message = IdRequest.ToBinary(header, idSource);
                BytesView?.Invoke(7, SendReceive.Send, message);

                var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
                DefaultMessage answer = await taskCompletionSource.Task.ConfigureAwait(false) as DefaultMessage;
                BytesView?.Invoke(7, SendReceive.Receive, answer.GetBytes());

                return answer;
            }
            else
            {
                MessageHeader header = new MessageHeader(0, 0, 7, 1, 0);
                DefaultMessage answer = new DefaultMessage(header);
                return answer;
            }
        }


        //Шифр 8
        public async Task<GetFFTResponce> GetFFT(double CentralFreqMHz)
        {
            if (client == null)
                return null;
            if (client.Connected)
            {
                MessageHeader header = GetMessageHeader(code: 8, length: GetFFTRequest.BinarySize - MessageHeader.BinarySize);
                header.SenderAddress = ReceiverAddress;
                header.ReceiverAddress = 0;

                byte[] message = GetFFTRequest.ToBinary(header, CentralFreqMHz);
                BytesView?.Invoke(8, SendReceive.Send, message);

                var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
                GetFFTResponce answer = await taskCompletionSource.Task.ConfigureAwait(false) as GetFFTResponce;
                BytesView?.Invoke(8, SendReceive.Receive, answer.GetBytes());

                return answer;
            }
            else
            {
                MessageHeader header = new MessageHeader(0, 0, 8, 1, 0);
                GetFFTResponce answer = new GetFFTResponce(header, new double[] { });
                return answer;
            }
        }

        //Шифр 9
        public async Task<DefaultMessage> SetUSRPn310Gain(byte ValueGain)
        {
            if (client == null)
                return null;
            if (client.Connected)
            {
                MessageHeader header = GetMessageHeader(code: 9, length: SetUSRPn310GainRequest.BinarySize - MessageHeader.BinarySize);
                header.SenderAddress = ReceiverAddress;
                header.ReceiverAddress = 0;

                byte[] message = SetUSRPn310GainRequest.ToBinary(header, ValueGain);
                BytesView?.Invoke(9, SendReceive.Send, message);

                var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
                DefaultMessage answer = await taskCompletionSource.Task.ConfigureAwait(false) as DefaultMessage;
                BytesView?.Invoke(9, SendReceive.Receive, answer.GetBytes());

                return answer;
            }
            else
            {
                MessageHeader header = new MessageHeader(0, 0, 9, 1, 0);
                DefaultMessage answer = new DefaultMessage(header);
                return answer;
            }
        }

        //Шифр 10
        public async Task<DefaultMessage> ChooseUSRPn310Chanel(byte ChanelNumber)
        {
            if (client == null)
                return null;
            if (client.Connected)
            {
                MessageHeader header = GetMessageHeader(code: 10, length: ChooseUSRPn310ChanelRequest.BinarySize - MessageHeader.BinarySize);
                header.SenderAddress = ReceiverAddress;
                header.ReceiverAddress = 0;

                byte[] message = ChooseUSRPn310ChanelRequest.ToBinary(header, ChanelNumber);
                BytesView?.Invoke(10, SendReceive.Send, message);

                var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
                DefaultMessage answer = await taskCompletionSource.Task.ConfigureAwait(false) as DefaultMessage;
                BytesView?.Invoke(10, SendReceive.Receive, answer.GetBytes());

                return answer;
            }
            else
            {
                MessageHeader header = new MessageHeader(0, 0, 10, 1, 0);
                DefaultMessage answer = new DefaultMessage(header);
                return answer;
            }
        }

        //Шифр 11
        public async Task<DefaultMessage> OnOffAGC(byte OnOff)
        {
            if (client == null)
                return null;
            if (client.Connected)
            {
                MessageHeader header = GetMessageHeader(code: 11, length: OnOffAGCRequest.BinarySize - MessageHeader.BinarySize);
                header.SenderAddress = ReceiverAddress;
                header.ReceiverAddress = 0;

                byte[] message = OnOffAGCRequest.ToBinary(header, OnOff);
                BytesView?.Invoke(11, SendReceive.Send, message);

                var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
                DefaultMessage answer = await taskCompletionSource.Task.ConfigureAwait(false) as DefaultMessage;
                BytesView?.Invoke(11, SendReceive.Receive, answer.GetBytes());

                return answer;
            }
            else
            {
                MessageHeader header = new MessageHeader(0, 0, 11, 1, 0);
                DefaultMessage answer = new DefaultMessage(header);
                return answer;
            }
        }


        ////Шифр 12 для Грозы-Z1
        //public async Task<GetNewObjectsListResponse> RequestNewObjectsOnRange(FreqCode rangeCode)
        //{
        //    if (client == null)
        //        return null;
        //    if (client.Connected)
        //    {
        //        MessageHeader header = GetMessageHeader(code: 12, length: FoundedNewObjectsRequest.BinarySize - MessageHeader.BinarySize);
        //        header.SenderAddress = ReceiverAddress;
        //        header.ReceiverAddress = 0;

        //        byte[] message = FoundedNewObjectsRequest.ToBinary(header, (byte)rangeCode);
        //        BytesView?.Invoke(12, SendReceive.Send, message);

        //        var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
        //        GetNewObjectsListResponse answer = await taskCompletionSource.Task.ConfigureAwait(false) as GetNewObjectsListResponse;
        //        BytesView?.Invoke(12, SendReceive.Receive, answer.GetBytes());

        //        return answer;
        //    }
        //    else
        //    {
        //        MessageHeader header = new MessageHeader(0, 0, 12, 1, 0);
        //        GetNewObjectsListResponse answer = new GetNewObjectsListResponse(header, null);
        //        return answer;
        //    }
        //}

        public enum FreqCode : byte
        {
            Both = 0,
            f2400 = 1,
            f5800 = 2
        }

        //Шифр 12 для Грозы-С
        public async Task<GetNewSignalsResponse> RequestNewSignals(FreqCode rangeCode)
        {
            if (client == null)
                return null;
            if (client.Connected)
            {
                MessageHeader header = GetMessageHeader(code: 12, length: FoundedNewObjectsRequest.BinarySize - MessageHeader.BinarySize);
                header.SenderAddress = ReceiverAddress;
                header.ReceiverAddress = 0;

                byte[] message = FoundedNewObjectsRequest.ToBinary(header, (byte)rangeCode);
                BytesView?.Invoke(12, SendReceive.Send, message);

                var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
                GetNewSignalsResponse answer = await taskCompletionSource.Task.ConfigureAwait(false) as GetNewSignalsResponse;
                BytesView?.Invoke(12, SendReceive.Receive, answer.GetBytes());

                return answer;
            }
            else
            {
                MessageHeader header = new MessageHeader(0, 0, 12, 1, 0);
                GetNewSignalsResponse answer = new GetNewSignalsResponse(header, 0, 0);
                return answer;
            }
        }



        //Шифр 17 для Грозы-С
        public async Task<GetNewDetailSignalsResponse> RequestNewSignalsDetail(FreqCode rangeCode)
        {
            if (client == null)
                return null;
            if (client.Connected)
            {
                MessageHeader header = GetMessageHeader(code: 17, length: FoundedNewObjectsRequest.BinarySize - MessageHeader.BinarySize);
                header.SenderAddress = ReceiverAddress;
                header.ReceiverAddress = 0;

                byte[] message = FoundedNewObjectsRequest.ToBinary(header, (byte)rangeCode);
                BytesView?.Invoke(17, SendReceive.Send, message);

                var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
                GetNewDetailSignalsResponse answer = await taskCompletionSource.Task.ConfigureAwait(false) as GetNewDetailSignalsResponse;
                BytesView?.Invoke(17, SendReceive.Receive, answer.GetBytes());

                return answer;
            }
            else
            {
                MessageHeader header = new MessageHeader(0, 0, 17, 1, 0);
                GetNewDetailSignalsResponse answer = new GetNewDetailSignalsResponse(header, 0, 0, 0, 0,0,0);
                return answer;
            }
        }



        //Шифр 13
        public async Task<DefaultMessage> RebootUsrp()
        {
            if (client == null)
                return null;
            if (client.Connected)
            {
                MessageHeader header = GetMessageHeader(code: 13, length: 0);
                header.SenderAddress = ReceiverAddress;
                header.ReceiverAddress = 0;

                byte[] message = DefaultMessage.ToBinary(header);
                BytesView?.Invoke(13, SendReceive.Send, message);

                var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
                DefaultMessage answer = await taskCompletionSource.Task.ConfigureAwait(false) as DefaultMessage;
                BytesView?.Invoke(13, SendReceive.Receive, answer.GetBytes());

                return answer;
            }
            else
            {
                MessageHeader header = new MessageHeader(0, 0, 13, 1, 0);
                DefaultMessage answer = new DefaultMessage(header);
                return answer;
            }
        }

        //Шифр 14 - см. Events

        //Шифр 15 - см. Events

        //Шифр 16
        public async Task<DefaultMessage> RecordUAVSignal(int TOwnUAV, int TOwnUAVFreq, double FreqMinMHz, float BandwidthMHz, byte Gain)
        {
            if (client == null)
                return null;
            if (client.Connected)
            {
                MessageHeader header = GetMessageHeader(code: 16, length: RecordUAVSignalRequest.BinarySize - MessageHeader.BinarySize);
                header.SenderAddress = ReceiverAddress;
                header.ReceiverAddress = 0;

                byte[] message = RecordUAVSignalRequest.ToBinary(header, TOwnUAV, TOwnUAVFreq, FreqMinMHz, BandwidthMHz, Gain);
                BytesView?.Invoke(16, SendReceive.Send, message);

                var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
                DefaultMessage answer = await taskCompletionSource.Task.ConfigureAwait(false) as DefaultMessage;
                BytesView?.Invoke(16, SendReceive.Receive, answer.GetBytes());

                return answer;
            }
            else
            {
                MessageHeader header = new MessageHeader(0, 0, 16, 1, 0);
                DefaultMessage answer = new DefaultMessage(header);
                return answer;
            }
        }
    }
}