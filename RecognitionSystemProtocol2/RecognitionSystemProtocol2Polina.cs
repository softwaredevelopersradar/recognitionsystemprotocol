using System;
using System.Collections.Generic;
using llcss;

namespace RecognitionSystemProtocol2
{
    public class NewObject : IBinarySerializable
    {
        public byte Signal;
        public double FreqMinMHz;
        public float BandwidthMHz;

        public const int BinarySize = 13;
        public int StructureBinarySize { get { return BinarySize; } }

        public const int NullValueBinarySize = BinarySize;

        public NewObject(byte Signal, double FreqMinMHz, float BandwidthMHz)
        {
            this.Signal = Signal;
            this.FreqMinMHz = FreqMinMHz;
            this.BandwidthMHz = BandwidthMHz;
        }


        public NewObject()
        {
        }

        public static byte[] ToBinary(byte Signal, double FreqMinMHz, float BandwidthMHz)
        {
            return new NewObject(Signal, FreqMinMHz, BandwidthMHz).GetBytes();
        }
        public void Decode(byte[] buffer, int shiftIndex)
        {
            Signal = buffer[shiftIndex];
            shiftIndex += 1;

            FreqMinMHz = BitConverter.ToDouble(buffer, shiftIndex);
            shiftIndex += 8;

            BandwidthMHz = BitConverter.ToSingle(buffer, shiftIndex);
            shiftIndex += 4;

        }

        public byte[] GetBytes()
        {
            var buffer = new byte[StructureBinarySize];
            GetBytes(buffer, 0);
            return buffer;
        }

        public bool TryGetBytes(out byte[] result)
        {
            try
            {
                result = GetBytes();
                return true;
            }
            catch
            {
                result = null;
                return false;
            }
        }

        public void GetBytes(byte[] buffer, int shiftIndex)
        {
            buffer[shiftIndex] = Signal;
            shiftIndex += 1;

            BitConverter.GetBytes(FreqMinMHz).CopyTo(buffer, shiftIndex);
            shiftIndex += 8;

            BitConverter.GetBytes(BandwidthMHz).CopyTo(buffer, shiftIndex);
            shiftIndex += 4;
        }

        public static bool IsValid(byte[] buffer, int startIndex = 0)
        {
            return IsValid(buffer, ref startIndex);
        }

        public static bool IsValid(byte[] buffer, ref int startIndex)
        {
            var tempIndex = 0;
            if (buffer == null)
            {
                return false;
            }
            if (buffer.Length < startIndex + BinarySize)
            {
                return false;
            }
            startIndex += 1;

            startIndex += 8;

            startIndex += 4;

            return true;
        }

        public static NewObject Parse(byte[] buffer, int startIndex = 0, bool validate = false)
        {
            if (validate && !IsValid(buffer))
            {
                throw new ArgumentException("buffer parse error");
            }
            var result = new NewObject();
            result.Decode(buffer, startIndex);
            return result;
        }

        public static bool TryParse(byte[] buffer, out NewObject result)
        {
            if (!IsValid(buffer))
            {
                result = null;
                return false;
            }
            try
            {
                result = new NewObject();
                result.Decode(buffer, 0);
                return true;
            }
            catch
            {
                result = null;
                return false;
            }
        }
    }

    public class GetNewObjectsListResponse : IBinarySerializable
    {
        public MessageHeader Header;
        public NewObject[] NewObjects;

        public int StructureBinarySize
        {
            get
            {
                var size = 8;
                for (int i = 0; i < Header.InformationLength / NewObject.BinarySize; ++i)
                {
                    size += NewObjects[i].StructureBinarySize;
                }

                return size;
            }
        }

        public const int NullValueBinarySize = MessageHeader.NullValueBinarySize + 0;

        public GetNewObjectsListResponse(MessageHeader Header, NewObject[] NewObjects)
        {
            this.Header = Header;
            this.NewObjects = NewObjects;
        }


        public GetNewObjectsListResponse()
        {
            Header = new MessageHeader();
        }

        public static byte[] ToBinary(MessageHeader Header, NewObject[] NewObjects)
        {
            return new GetNewObjectsListResponse(Header, NewObjects).GetBytes();
        }


        public void Decode(byte[] buffer, int shiftIndex)
        {
            Header = MessageHeader.Parse(buffer, shiftIndex);
            shiftIndex += 8;

            NewObjects = new NewObject[Header.InformationLength / NewObject.BinarySize];
            for (int i = 0; i < Header.InformationLength / NewObject.BinarySize; ++i)
            {
                NewObjects[i] = NewObject.Parse(buffer, shiftIndex);
                shiftIndex += NewObjects[i].StructureBinarySize;
            }
        }

        public byte[] GetBytes()
        {
            var buffer = new byte[StructureBinarySize];
            GetBytes(buffer, 0);
            return buffer;
        }

        public bool TryGetBytes(out byte[] result)
        {
            try
            {
                result = GetBytes();
                return true;
            }
            catch
            {
                result = null;
                return false;
            }
        }


        public void GetBytes(byte[] buffer, int shiftIndex)
        {
            Header.GetBytes(buffer, shiftIndex);
            shiftIndex += 8;

            for (int i = 0; i < Header.InformationLength / NewObject.BinarySize; ++i)
            {
                NewObjects[i].GetBytes(buffer, shiftIndex);
                shiftIndex += NewObjects[i].StructureBinarySize;
            }
        }

        public static bool IsValid(byte[] buffer, int startIndex = 0)
        {
            return IsValid(buffer, ref startIndex);
        }

        public static bool IsValid(byte[] buffer, ref int startIndex)
        {
            var tempIndex = 0;
            if (buffer == null)
            {
                return false;
            }
            tempIndex = startIndex;
            if (!MessageHeader.IsValid(buffer, ref startIndex))
            {
                return false;
            }

            var Header = new MessageHeader();
            Header.Decode(buffer, tempIndex);
            tempIndex = startIndex;

            startIndex += (int)(Header.InformationLength / NewObject.BinarySize) * NewObject.BinarySize;
            if (buffer.Length < startIndex)
            {
                return false;
            }

            return true;
        }

        public static GetNewObjectsListResponse Parse(byte[] buffer, int startIndex = 0, bool validate = false)
        {
            if (validate && !IsValid(buffer))
            {
                throw new ArgumentException("buffer parse error");
            }
            var result = new GetNewObjectsListResponse();
            result.Decode(buffer, startIndex);
            return result;
        }

        public static bool TryParse(byte[] buffer, out GetNewObjectsListResponse result)
        {
            if (!IsValid(buffer))
            {
                result = null;
                return false;
            }
            try
            {
                result = new GetNewObjectsListResponse();
                result.Decode(buffer, 0);
                return true;
            }
            catch
            {
                result = null;
                return false;
            }
        }
    }

    public class IdRequest : IBinarySerializable
    {
        public MessageHeader Header;
        public int Id;

        public const int BinarySize = 12;
        public int StructureBinarySize { get { return BinarySize; } }

        public const int NullValueBinarySize = BinarySize;

        public IdRequest(MessageHeader Header, int ID)
        {
            this.Header = Header;
            this.Id = ID;
        }


        public IdRequest()
        {
            Header = new MessageHeader();
        }

        public static byte[] ToBinary(MessageHeader Header, int ID)
        {
            return new IdRequest(Header, ID).GetBytes();
        }
        public void Decode(byte[] buffer, int shiftIndex)
        {
            Header = MessageHeader.Parse(buffer, shiftIndex);
            shiftIndex += 8;

            Id = BitConverter.ToInt32(buffer, shiftIndex);
            shiftIndex += 4;
        }

        public byte[] GetBytes()
        {
            var buffer = new byte[StructureBinarySize];
            GetBytes(buffer, 0);
            return buffer;
        }

        public bool TryGetBytes(out byte[] result)
        {
            try
            {
                result = GetBytes();
                return true;
            }
            catch
            {
                result = null;
                return false;
            }
        }

        public void GetBytes(byte[] buffer, int shiftIndex)
        {
            Header.GetBytes(buffer, shiftIndex);
            shiftIndex += 8;

            BitConverter.GetBytes(Id).CopyTo(buffer, shiftIndex);
            shiftIndex += 4;
        }

        public static bool IsValid(byte[] buffer, int startIndex = 0)
        {
            return IsValid(buffer, ref startIndex);
        }

        public static bool IsValid(byte[] buffer, ref int startIndex)
        {
            var tempIndex = 0;
            if (buffer == null)
            {
                return false;
            }
            if (buffer.Length < startIndex + BinarySize)
            {
                return false;
            }
            tempIndex = startIndex;
            if (!MessageHeader.IsValid(buffer, ref startIndex))
            {
                return false;
            }

            var Header = new MessageHeader();
            Header.Decode(buffer, tempIndex);
            startIndex += 4;

            return true;
        }

        public static IdRequest Parse(byte[] buffer, int startIndex = 0, bool validate = false)
        {
            if (validate && !IsValid(buffer))
            {
                throw new ArgumentException("buffer parse error");
            }
            var result = new IdRequest();
            result.Decode(buffer, startIndex);
            return result;
        }

        public static bool TryParse(byte[] buffer, out IdRequest result)
        {
            if (!IsValid(buffer))
            {
                result = null;
                return false;
            }
            try
            {
                result = new IdRequest();
                result.Decode(buffer, 0);
                return true;
            }
            catch
            {
                result = null;
                return false;
            }
        }
    }
}
