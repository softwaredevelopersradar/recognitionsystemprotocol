﻿using llcss;
using Nito.AsyncEx;
using RecognitionSystemProtocol2;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace RecognitionSystemProtocol2
{
    public class RecognitionSystemServer
    {
        private int port = 36000;
        private TcpListener listener;

        private List<ClientObject> ListOfClientObjects = new List<ClientObject>();

        public delegate void ResponseIBinarySerializableServerRequest(int Response, IBinarySerializable Request);
        public event ResponseIBinarySerializableServerRequest ServerRequest;

        List<(int, int)> Responses = new List<(int, int)>();

        public async Task ServerStart()
        {
            listener = new TcpListener(IPAddress.Parse("127.0.0.1"), port);
            listener.Start();
            Console.WriteLine($"Server started on {IPAddress.Parse("127.0.0.1")}:{port}");
            Console.WriteLine("Waiting for connections...");
            while (true)
            {
                TcpClient client = await listener.AcceptTcpClientAsync();
                ClientObject clientObject = new ClientObject(client);
                ListOfClientObjects.Add(clientObject);

                InitClientObjectEvents(ref clientObject);

                Console.WriteLine("Client connected");

                //создаем новый поток для обслуживания нового клиента
                Task.Run(() => clientObject.Read());
                var answer = await ServerSendAddressIdentificationRequest();
            }
        }

        public async Task ServerStart(string IP, int port)
        {
            try
            {
                //Пробуем стартовать сервер
                listener = new TcpListener(IPAddress.Parse(IP), port);
                listener.Start();
                Console.WriteLine($"Server started on {IP}:{port}");
                Console.WriteLine("Waiting for connections...");
            }
            catch
            {
                listener = new TcpListener(IPAddress.Parse("127.0.0.1"), port);
                listener.Start();
                Console.WriteLine($"Server started on {IPAddress.Parse("127.0.0.1")}:{port}");
                Console.WriteLine("Waiting for connections...");
            }
            while (true)
            {
                TcpClient client = await listener.AcceptTcpClientAsync();
                ClientObject clientObject = new ClientObject(client);
                ListOfClientObjects.Add(clientObject);

                InitClientObjectEvents(ref clientObject);

                Console.WriteLine("Client connected");

                //создаем новый поток для обслуживания нового клиента
                Task.Run(() => clientObject.Read());
                var answer = await ServerSendAddressIdentificationRequest();
            }
        }

        private void InitClientObjectEvents(ref ClientObject clientObject)
        {
            clientObject.Request += ClientObject_Request;
            clientObject.ClientIsConnected += ClientObject_ClientIsConnected;
        }

        private void ClientObject_ClientIsConnected(object sender, bool isConnected)
        {
            if (!isConnected)
            {
                var obj = (ClientObject)sender;
                obj.Request -= ClientObject_Request;
                obj.ClientIsConnected -= ClientObject_ClientIsConnected;
                ListOfClientObjects.Remove(obj);
            }
        }

        private void ClientObject_Request(int Adress, int Response, IBinarySerializable Request)
        {
            lock (Responses)
            {
                Responses.Add((Response, Adress));
            }
            ServerRequest?.Invoke(Response, Request);
        }

        //Шифр 0
        public async Task<List<DefaultMessage>> ServerSendAddressIdentificationRequest()
        {
            List<DefaultMessage> ListOfDefaultMessages = new List<DefaultMessage>();
            for (int i = 0; i < ListOfClientObjects.Count; i++)
            {
                var result = await ListOfClientObjects[i].SendAddressIdentificationRequest(Convert.ToByte(i + 1));
                ListOfDefaultMessages.Add(result);
            }
            return ListOfDefaultMessages;
        }

        //Тест
        public async Task<DefaultMessage> ServerSendTestResponse(byte[] bytes)
        {
            var result = await ListOfClientObjects.First().SendTestResponse(bytes);
            return result;
        }

        //Шифр 1
        public async Task<DefaultMessage> ServerSendGetActiveListFriendlyUAVsResponse(IEnumerable<BPLA> iBplas)
        {
            int i = 0;
            lock (Responses)
            {
                var el = Responses.First(x => x.Item1 == 1);
                i = el.Item2 - 1;
                Responses.Remove(el);
            }

            var result = await ListOfClientObjects[i].SendGetActiveListFriendlyUAVsResponse(iBplas);
            return result;
        }

        //Шифр 2
        public async Task<DefaultMessage> ServerSendAddFriendlyUAVResponse()
        {
            int i = 0;
            lock (Responses)
            {
                var el = Responses.First(x => x.Item1 == 2);
                i = el.Item2 - 1;
                Responses.Remove(el);
            }

            var result = await ListOfClientObjects[i].SendAddFriendlyUAVResponse();
            return result;
        }

        //Шифр 3
        public async Task<DefaultMessage> ServerSendDeleteFriendlyUAVResponse()
        {
            int i = 0;
            lock (Responses)
            {
                var el = Responses.First(x => x.Item1 == 3);
                i = el.Item2 - 1;
                Responses.Remove(el);
            }

            var result = await ListOfClientObjects[i].SendDeleteFriendlyUAVResponse();
            return result;
        }

        //Шифр 4
        public async Task<DefaultMessage> ServerSendDeleteFriendlyUAVsResponse()
        {
            int i = 0;
            lock (Responses)
            {
                var el = Responses.First(x => x.Item1 == 4);
                i = el.Item2 - 1;
                Responses.Remove(el);
            }

            var result = await ListOfClientObjects[i].SendDeleteFriendlyUAVsResponse();
            return result;
        }

        //Шифр 5
        public async Task<DefaultMessage> ServerSendAffiliationUAVResponse(UAVType Type, IEnumerable<byte> ID)
        {
            int i = 0;
            lock (Responses)
            {
                var el = Responses.First(x => x.Item1 == 5);
                i = el.Item2 - 1;
                Responses.Remove(el);
            }

            var result = await ListOfClientObjects[i].SendAffiliationUAVResponse(Type, ID);
            return result;
        }
        //Шифр дефолт
        public async Task<DefaultMessage> ServerDefaultResponse(byte code)
        {
            int i = 0;
            lock (Responses)
            {
                var el = Responses.First(x => x.Item1 == code);
                i = el.Item2 - 1;
                Responses.Remove(el);
            }

            var result = await ListOfClientObjects[i].SendDefaultResponse(code);
            return result;
        }

        //Шифр 9
        public async Task<DefaultMessage> ServerGainResponse()
        {
            int i = 0;
            lock (Responses)
            {
                var el = Responses.First(x => x.Item1 == 9);
                i = el.Item2 - 1;
                Responses.Remove(el);
            }

            var result = await ListOfClientObjects[i].SendGainResponse();
            return result;
        }

        ////Шифр 5
        //public async Task<DefaultMessage> ServerSendNewObjectsResponse(IEnumerable<NewObject> objects)
        //{
        //    int i = 0;
        //    lock (Responses)
        //    {
        //        var el = Responses.First(x => x.Item1 == 12);
        //        i = el.Item2 - 1;
        //        Responses.Remove(el);
        //    }

        //    var result = await ListOfClientObjects[i].SendNewObjects(objects);
        //    return result;
        //}

        //Шифр 12
        public async Task<DefaultMessage> ServerSendNewObjectsResponse(byte signal1, byte signal2)
        {
            int i = 0;
            lock (Responses)
            {
                var el = Responses.First(x => x.Item1 == 12);
                i = el.Item2 - 1;
                Responses.Remove(el);
            }

            var result = await ListOfClientObjects[i].SendNewObjects(signal1, signal2);
            return result;
        }

        //Шифр 13
        public async Task<DefaultMessage> ServerFinishedRebooting()
        {
            int i = 0;
            lock (Responses)
            {
                var el = Responses.First(x => x.Item1 == 13);
                i = el.Item2 - 1;
                Responses.Remove(el);
            }
            Thread.Sleep(5000);
            var result = await ListOfClientObjects[i].SendFinishRebooting();
            return result;
        }
    }

    public class ClientObject
    {
        public TcpClient client;

        private readonly AsyncLock asyncLock;

        //public delegate void IsConnectedEventHandler(bool isConnected);
        public EventHandler<bool> ClientIsConnected;

        public delegate void OnReadEventHandler(bool isRead);
        public event OnReadEventHandler ClientIsRead;

        public delegate void IsWriteEventHandler(bool isWrite);
        public event IsWriteEventHandler ClientIsWrite;

        public delegate void AdressAndResponseIBinarySerializableRequest(int Adress, int Response, IBinarySerializable Request);
        public event AdressAndResponseIBinarySerializableRequest Request;

        public int ClientAdress = 0;

        public ClientObject(TcpClient tcpClient)
        {
            client = tcpClient;
            asyncLock = new AsyncLock();
        }


        public async Task Read()
        {
            while (client.Connected)
            {
                var headerBuffer = new byte[MessageHeader.BinarySize];
                var header = new MessageHeader();
                var count = 0;

                try
                {
                    count = await client.GetStream().ReadAsync(headerBuffer, 0, MessageHeader.BinarySize).ConfigureAwait(false);
                    ClientIsRead?.Invoke(true);
                }
                catch (Exception)
                {
                    //Console.WriteLine("Сервер отвалился");
                    ClientIsRead?.Invoke(false);
                    ClientIsConnected?.Invoke(this, false);
                    //break;
                }
                if (count != MessageHeader.BinarySize)
                {
                    //fatal error
                    //break;
                    ClientIsRead?.Invoke(false);
                    return;
                }

                MessageHeader.TryParse(headerBuffer, out header);

                switch (header.Code)
                {
                    case 0:
                        Console.WriteLine(headerBuffer);
                        break;
                    //case 1:
                    //    var response1 = await ReadResponse<SetActiveListFriendlyUAVsRequest>(header, headerBuffer).ConfigureAwait(false);
                    //    Request?.Invoke(ClientAdress, header.Code, response1);
                    //    break;
                    case 1:
                        var response1 = await ReadResponse<DefaultMessage>(header, headerBuffer).ConfigureAwait(false);
                        Request?.Invoke(ClientAdress, header.Code, response1);
                        break;
                    //case 2:
                    //    var response2 = await ReadResponse<AddFriendlyUAVRequest>(header, headerBuffer).ConfigureAwait(false);
                    //    Request?.Invoke(ClientAdress, header.Code, response2);
                    //    break;
                    case 3:
                        var response3 = await ReadResponse<DeleteFriendlyUAVRequest>(header, headerBuffer).ConfigureAwait(false);
                        Request?.Invoke(ClientAdress, header.Code, response3);
                        break;
                    case 4:
                        var response4 = await ReadResponse<DefaultMessage>(header, headerBuffer).ConfigureAwait(false);
                        Request?.Invoke(ClientAdress, header.Code, response4);
                        break;
                    case 5:
                        var response5 = await ReadResponse<AffiliationUAVRequest>(header, headerBuffer).ConfigureAwait(false);
                        Request?.Invoke(ClientAdress, header.Code, response5);
                        break;
                    case 6:
                        //var response7 = await ReadResponse<AffiliationUAVRequest>(header, headerBuffer).ConfigureAwait(false);
                        var response6 = await ReadResponse<IdRequest>(header, headerBuffer).ConfigureAwait(false);
                        Request?.Invoke(ClientAdress, header.Code, response6);
                        break;
                    case 7:
                        //var response7 = await ReadResponse<AffiliationUAVRequest>(header, headerBuffer).ConfigureAwait(false);
                        var response7 = await ReadResponse<IdRequest>(header, headerBuffer).ConfigureAwait(false);
                        Request?.Invoke(ClientAdress, header.Code, response7);
                        break;
                    case 8:
                        var response8 = await ReadResponse<GetFFTRequest>(header, headerBuffer).ConfigureAwait(false);
                        Request?.Invoke(ClientAdress, header.Code, response8);
                        break;
                    case 9:
                        var response9 = await ReadResponse<DefaultMessage>(header, headerBuffer).ConfigureAwait(false);
                        Request?.Invoke(ClientAdress, header.Code, response9);
                        break;
                    case 12:
                        var response12 = await ReadResponse<DefaultMessage>(header, headerBuffer).ConfigureAwait(false);
                        Request?.Invoke(ClientAdress, header.Code, response12);
                        break;
                    case 13:
                        var response13 = await ReadResponse<DefaultMessage>(header, headerBuffer).ConfigureAwait(false);
                        Request?.Invoke(ClientAdress, header.Code, response13);
                        break;
                    case 16:
                        //var response7 = await ReadResponse<AffiliationUAVRequest>(header, headerBuffer).ConfigureAwait(false);
                        var response16 = await ReadResponse<RecordUAVSignalRequest>(header, headerBuffer).ConfigureAwait(false);
                        Request?.Invoke(ClientAdress, header.Code, response16);
                        break;
                    case 17:
                        var response17 = await ReadResponse<DefaultMessage>(header, headerBuffer).ConfigureAwait(false);
                        Request?.Invoke(ClientAdress, header.Code, response17);
                        break;
                    default:
                        //вывод полезной информации
                        break;
                }
            }
        }


        private async Task<T> ReadResponse<T>(MessageHeader header, byte[] headerBuffer) where T : class, IBinarySerializable, new()
        {
            var count = 0;
            var buffer = new byte[MessageHeader.BinarySize + header.InformationLength];
            headerBuffer.CopyTo(buffer, 0);

            if (header.InformationLength != 0)
            {
                var difference = header.InformationLength;
                var shiftpos = 0;

                while (difference != 0)
                {
                    count = await client.GetStream().ReadAsync(buffer, MessageHeader.BinarySize + shiftpos, difference).ConfigureAwait(false);
                    if (ClientIsRead != null)
                        ClientIsRead(true);
                    shiftpos += count;
                    difference = difference - count;
                }
            }

            try
            {
                var result = new T();
                result.Decode(buffer, 0);
                return result;
            }
            catch (Exception)
            {
                return null;
            }
        }

        static MessageHeader GetMessageHeader(int code, int length)
        {
            return new MessageHeader(0, 1, (byte)code, 0, length);
        }

        private async Task SendDefault(MessageHeader header)
        {
            using (await asyncLock.LockAsync().ConfigureAwait(false))
            {
                try
                {
                    await client.GetStream().WriteAsync(header.GetBytes(), 0, MessageHeader.BinarySize).ConfigureAwait(false);
                    ClientIsWrite?.Invoke(true);
                }
                catch (Exception)
                {
                    ClientIsWrite?.Invoke(false);
                }
            }
        }

        private async Task SendRequest(MessageHeader header, byte[] message)
        {
            using (await asyncLock.LockAsync().ConfigureAwait(false))
            {
                int count = message.Count();
                try
                {
                    await client.GetStream().WriteAsync(message, 0, count).ConfigureAwait(false);
                    if (ClientIsWrite != null)
                        ClientIsWrite(true);
                }
                catch (Exception)
                {
                    if (ClientIsWrite != null)
                        ClientIsWrite(false);
                }
            }
        }

        private async Task SendResponse<T>(T Response) where T : class, IBinarySerializable, new()
        {
            using (await asyncLock.LockAsync().ConfigureAwait(false))
            {
                try
                {
                    await client.GetStream().WriteAsync(Response.GetBytes(), 0, Response.StructureBinarySize).ConfigureAwait(false);
                    if (ClientIsWrite != null)
                        ClientIsWrite(true);
                }
                catch (Exception)
                {
                    if (ClientIsWrite != null)
                        ClientIsWrite(false);
                }
            }
        }

        //Шифр 0 
        public async Task<DefaultMessage> SendAddressIdentificationRequest(byte ReceiverAddress)
        {
            if (client == null)
                return null;
            if (client.Connected)
            {
                ClientAdress = ReceiverAddress;

                MessageHeader header = GetMessageHeader(code: 0, length: 0);
                header.ReceiverAddress = ReceiverAddress;
                byte[] message = DefaultMessage.ToBinary(header);

                await SendRequest(header, message).ConfigureAwait(false);
                DefaultMessage answer = new DefaultMessage(header);
                return answer;
            }
            else
            {
                MessageHeader header = new MessageHeader(0, 0, 0, 0, 0);
                DefaultMessage answer = new DefaultMessage(header);
                return answer;
            }
        }

        //Тест
        public async Task<DefaultMessage> SendTestResponse(byte[] bytes)
        {
            await SendRequest(new MessageHeader(), bytes).ConfigureAwait(false);
            DefaultMessage answer = new DefaultMessage();
            return answer;
        }

        //Шифр 1
        public async Task<DefaultMessage> SendGetActiveListFriendlyUAVsResponse(IEnumerable<BPLA> iBplas)
        {
            if (client == null)
                return null;
            if (client.Connected)
            {
                MessageHeader header = GetMessageHeader(code: 1, length: IsFileSignalUAV.BinarySize * iBplas.Count());

                byte[] message = GetActiveListFriendlyUAVsResponse.ToBinary(header, (byte)iBplas.Count(), iBplas.ToArray());

                await SendRequest(header, message).ConfigureAwait(false);
                DefaultMessage answer = new DefaultMessage(header);
                return answer;
            }
            else
            {
                MessageHeader header = new MessageHeader(0, 0, 1, 1, 0);
                DefaultMessage answer = new DefaultMessage(header);
                return answer;
            }
        }

        //Шифр 2
        public async Task<DefaultMessage> SendAddFriendlyUAVResponse()
        {
            if (client == null)
                return null;
            if (client.Connected)
            {
                MessageHeader header = GetMessageHeader(code: 2, length: 0);

                byte[] message = DefaultMessage.ToBinary(header);

                await SendRequest(header, message).ConfigureAwait(false);
                DefaultMessage answer = new DefaultMessage(header);
                return answer;
            }
            else
            {
                MessageHeader header = new MessageHeader(0, 0, 2, 1, 0);
                DefaultMessage answer = new DefaultMessage(header);
                return answer;
            }
        }

        //Шифр 3
        public async Task<DefaultMessage> SendDeleteFriendlyUAVResponse()
        {
            if (client == null)
                return null;
            if (client.Connected)
            {
                MessageHeader header = GetMessageHeader(code: 3, length: 0);

                byte[] message = DefaultMessage.ToBinary(header);

                await SendRequest(header, message).ConfigureAwait(false);
                DefaultMessage answer = new DefaultMessage(header);
                return answer;
            }
            else
            {
                MessageHeader header = new MessageHeader(0, 0, 3, 1, 0);
                DefaultMessage answer = new DefaultMessage(header);
                return answer;
            }
        }

        //Шифр 4
        public async Task<DefaultMessage> SendDeleteFriendlyUAVsResponse()
        {
            if (client == null)
                return null;
            if (client.Connected)
            {
                MessageHeader header = GetMessageHeader(code: 4, length: 0);

                byte[] message = DefaultMessage.ToBinary(header);

                await SendRequest(header, message).ConfigureAwait(false);
                DefaultMessage answer = new DefaultMessage(header);
                return answer;
            }
            else
            {
                MessageHeader header = new MessageHeader(0, 0, 4, 1, 0);
                DefaultMessage answer = new DefaultMessage(header);
                return answer;
            }
        }

        //Шифр 5
        public async Task<DefaultMessage> SendAffiliationUAVResponse(UAVType type, IEnumerable<byte> ID)
        {
            if (client == null)
                return null;
            if (client.Connected)
            {
                MessageHeader header = GetMessageHeader(code: 5, length: AffiliationUAVResponse.BinarySize - MessageHeader.BinarySize);

                //byte[] message = AffiliationUAVResponse.ToBinary(header, type, ID.ToArray());

                //await SendRequest(header, message).ConfigureAwait(false);
                DefaultMessage answer = new DefaultMessage(header);
                return answer;
            }
            else
            {
                MessageHeader header = new MessageHeader(0, 0, 5, 1, 0);
                DefaultMessage answer = new DefaultMessage(header);
                return answer;
            }
        }


        //общий
        public async Task<DefaultMessage> SendDefaultResponse(byte code)
        {
            if (client == null)
                return null;
            if (client.Connected)
            {
                MessageHeader header = GetMessageHeader(code: code, length: 0);
                header.ErrorCode = 6;
                byte[] message = DefaultMessage.ToBinary(header);

                await SendRequest(header, message).ConfigureAwait(false);
                DefaultMessage answer = new DefaultMessage(header);
                return answer;
            }
            else
            {
                MessageHeader header = new MessageHeader(0, 0, code, 1, 0);
                DefaultMessage answer = new DefaultMessage(header);
                return answer;
            }
        }

        //Шифр 9
        public async Task<DefaultMessage> SendGainResponse()
        {
            if (client == null)
                return null;
            if (client.Connected)
            {
                MessageHeader header = GetMessageHeader(code: 9, length: 0);

                byte[] message = DefaultMessage.ToBinary(header);

                await SendRequest(header, message).ConfigureAwait(false);
                DefaultMessage answer = new DefaultMessage(header);
                return answer;
            }
            else
            {
                MessageHeader header = new MessageHeader(0, 0, 9, 1, 0);
                DefaultMessage answer = new DefaultMessage(header);
                return answer;
            }
        }

        ////Шифр 12
        //public async Task<DefaultMessage> SendNewObjects(IEnumerable<NewObject> objects)
        //{
        //    if (client == null)
        //        return null;
        //    if (client.Connected)
        //    {
        //        MessageHeader header = GetMessageHeader(code: 12, length: NewObject.BinarySize * objects.Count());

        //        byte[] message = GetNewObjectsListResponse.ToBinary(header, objects.ToArray());

        //        await SendRequest(header, message).ConfigureAwait(false);
        //        DefaultMessage answer = new DefaultMessage(header);
        //        return answer;
        //    }
        //    else
        //    {
        //        MessageHeader header = new MessageHeader(0, 0, 12, 1, 0);
        //        DefaultMessage answer = new DefaultMessage(header);
        //        return answer;
        //    }
        //}

        //Шифр 12
        public async Task<DefaultMessage> SendNewObjects(byte signal1, byte signal2)
        {
            if (client == null)
                return null;
            if (client.Connected)
            {
                MessageHeader header = GetMessageHeader(code: 12, length: 2);

                byte[] message = GetNewSignalsResponse.ToBinary(header, signal1, signal2);

                await SendRequest(header, message).ConfigureAwait(false);
                DefaultMessage answer = new DefaultMessage(header);
                return answer;
            }
            else
            {
                MessageHeader header = new MessageHeader(0, 0, 12, 1, 0);
                DefaultMessage answer = new DefaultMessage(header);
                return answer;
            }
        }

        //Шифр 13
        public async Task<DefaultMessage> SendFinishRebooting()
        {
            if (client == null)
                return null;
            if (client.Connected)
            {
                MessageHeader header = GetMessageHeader(code: 13, length: 0);

                byte[] message = DefaultMessage.ToBinary(header);
                await SendRequest(header, message).ConfigureAwait(false);
                DefaultMessage answer = new DefaultMessage(header);
                return answer;
            }
            else
            {
                MessageHeader header = new MessageHeader(0, 0, 13, 1, 0);
                DefaultMessage answer = new DefaultMessage(header);
                return answer;
            }
        }
    }
}
