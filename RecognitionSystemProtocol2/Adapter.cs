﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RecognitionSystemProtocol2
{
    public static class UAVExtension
    {
        public static UAVandText ToUAVandText(this UAVandString uav)
        {
            return new UAVandText()
            {
                ID = Support.StringToCharBytes(uav.ID, UAVandText.IDLength),
                Name = Support.StringToCharBytes(uav.Name, UAVandText.NameLength),
                Note = Support.StringToCharBytes(uav.Note, UAVandText.NoteLength)
            };
        }

        public static UAVandString ToUAVandString(this UAVandText uav)
        {
            return new UAVandString()
            {
                ID = Support.BytesToString(uav.ID),
                Name = Support.BytesToString(uav.Name),
                Note = Support.BytesToString(uav.Note)
            };

        }

        public static GetActiveListFriendlyUAVsStringResponse ToGetActiveListFriendlyUAVsStringResponse(this GetActiveListFriendlyUAVsResponse getActiveListFriendlyUAVsResponse)
        {
            var getActiveListFriendlyUaVsStringResponse = new GetActiveListFriendlyUAVsStringResponse()
            {
                Header = getActiveListFriendlyUAVsResponse.Header,
                CountBPLA = getActiveListFriendlyUAVsResponse.CountBPLA,
            };

            List<BPLAString> bplaStrings = new List<BPLAString>(); 

            for (int i = 0; i < getActiveListFriendlyUAVsResponse.CountBPLA; i++)
            {
                BPLAString temp = new BPLAString()
                {
                    uav = getActiveListFriendlyUAVsResponse.BPLAs[i].uav.ToUAVandString(),
                    CountFreqs = getActiveListFriendlyUAVsResponse.BPLAs[i].CountFreqs,
                    ActiveAndTimePackage = getActiveListFriendlyUAVsResponse.BPLAs[i].ActiveAndTimePackage,
                };
                bplaStrings.Add(temp);
            }

            getActiveListFriendlyUaVsStringResponse.BPLAs = bplaStrings.ToArray();

            return getActiveListFriendlyUaVsStringResponse;
        }

    }

}
