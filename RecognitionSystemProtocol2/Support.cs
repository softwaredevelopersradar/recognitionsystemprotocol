﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RecognitionSystemProtocol2
{
    public static  class Support
    {
        public static byte[] StringToCharBytes(string value, int count = 0)
        {
            var charArray = value.ToCharArray();

            byte[] bytes = new byte[0];

            if (count == 0)
            {
                bytes = charArray.Select(x => Convert.ToByte(x)).ToArray();
            }
            else
            {
                if (count > 0)
                {
                    var bytesList = charArray.Take(count).Select(x => Convert.ToByte(x)).ToList();
                    while (bytesList.Count() < count)
                    {
                        bytesList.Add(Convert.ToByte(' '));
                    }
                    bytes = bytesList.ToArray();
                }
            }
            return bytes;
        }

        public static string BytesToString(IEnumerable<byte> bytes)
        {
            //StringBuilder sb = new StringBuilder();

            //for (int i = 0; i < bytes.Count(); i++)
            //{
            //    sb.Append(bytes.ElementAt(i).ToString());
            //}
            //return sb.ToString();

            byte[] myArray = new byte[bytes.ToList().Count];
            int i = 0;
            foreach (var d in bytes)
            {
                myArray[i++] = d;
            }

            string res = System.Text.Encoding.ASCII.GetString(myArray);
            return res;
        }
    }
}
